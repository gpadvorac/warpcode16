-- wcPropsTile


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcPropsTile

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTile_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTile_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTile_lst', 'spWcPropsTile_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTile_lst', 'spWcPropsTile_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTile_lst', 'spWcPropsTile_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTile_lst', 'spWcPropsTile_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTile_lst', 'spWcPropsTile_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTile_lst]
-- Script for this SP was created on: 12/27/2017 4:59:21 PM
(
@PrpTl_Tb_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
PrpTl_Id,
PrpTl_Tb_Id,
PrpTl_Sort,
PrpTl_GridName,
PrpTl_Header,
PrpTl_IsDefaultView,
PrpTl_IsActiveRow,
PrpTl_IsDeleted,
PrpTl_CreatedUserId,
PrpTl_CreatedDate,
PrpTl_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

PrpTl_LastModifiedDate,
PrpTl_Stamp


FROM 		wcPropsTile LEFT OUTER JOIN
                tbPerson ON wcPropsTile.PrpTl_UserId = tbPerson.Pn_SecurityUserId


WHERE   (PrpTl_Tb_Id = @PrpTl_Tb_Id) AND (PrpTl_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTile_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTile_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTile_row', 'spWcPropsTile_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTile_row', 'spWcPropsTile_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTile_row', 'spWcPropsTile_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTile_row', 'spWcPropsTile_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTile_row', 'spWcPropsTile_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTile_row]
-- Script for this SP was created on: 12/27/2017 4:59:21 PM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
PrpTl_Id,
PrpTl_Tb_Id,
PrpTl_Sort,
PrpTl_GridName,
PrpTl_Header,
PrpTl_IsDefaultView,
PrpTl_IsActiveRow,
PrpTl_IsDeleted,
PrpTl_CreatedUserId,
PrpTl_CreatedDate,
PrpTl_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

PrpTl_LastModifiedDate,
PrpTl_Stamp


FROM 		wcPropsTile LEFT OUTER JOIN
                tbPerson ON wcPropsTile.PrpTl_UserId = tbPerson.Pn_SecurityUserId


WHERE   (PrpTl_Id = @Id)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTile_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTile_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTile_lstAll', 'spWcPropsTile_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTile_lstAll', 'spWcPropsTile_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTile_lstAll', 'spWcPropsTile_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTile_lstAll', 'spWcPropsTile_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTile_lstAll', 'spWcPropsTile_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTile_lstAll]
-- Script for this SP was created on: 12/27/2017 4:59:21 PM
AS
SET NOCOUNT ON

SELECT 	
PrpTl_Id,
PrpTl_Tb_Id,
PrpTl_Sort,
PrpTl_GridName,
PrpTl_Header,
PrpTl_IsDefaultView,
PrpTl_IsActiveRow,
PrpTl_IsDeleted,
PrpTl_CreatedUserId,
PrpTl_CreatedDate,
PrpTl_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

PrpTl_LastModifiedDate,
PrpTl_Stamp


FROM 		wcPropsTile LEFT OUTER JOIN
                tbPerson ON wcPropsTile.PrpTl_UserId = tbPerson.Pn_SecurityUserId

WHERE   (PrpTl_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTile_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTile_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTile_putInsert', 'spWcPropsTile_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTile_putInsert', 'spWcPropsTile_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTile_putInsert', 'spWcPropsTile_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTile_putInsert', 'spWcPropsTile_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTile_putInsert', 'spWcPropsTile_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTile_putInsert]
-- Script for this SP was created on: 12/27/2017 4:59:21 PM
(
@PrpTl_Id uniqueidentifier = NULL OUTPUT,
@PrpTl_Tb_Id uniqueidentifier,
@PrpTl_Sort int,
@PrpTl_GridName varchar(50),
@PrpTl_Header varchar(75),
@PrpTl_IsDefaultView bit,
@PrpTl_IsActiveRow bit,
@PrpTl_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcPropsTile'
      INSERT INTO wcPropsTile
          (
		PrpTl_Id,
		PrpTl_Tb_Id,
		PrpTl_Sort,
		PrpTl_GridName,
		PrpTl_Header,
		PrpTl_IsDefaultView,
		PrpTl_IsActiveRow,
		PrpTl_CreatedUserId,
		PrpTl_CreatedDate,
		PrpTl_UserId,
		PrpTl_LastModifiedDate
				)
VALUES	(
		@PrpTl_Id, 
		@PrpTl_Tb_Id, 
		@PrpTl_Sort, 
		RTRIM(LTRIM(@PrpTl_GridName)), 
		RTRIM(LTRIM(@PrpTl_Header)), 
		@PrpTl_IsDefaultView, 
		@PrpTl_IsActiveRow, 
		@PrpTl_UserId, 
		GETDATE(), 
		@PrpTl_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Id', 'uniqueidentifier', @PrpTl_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Tb_Id', 'uniqueidentifier', @PrpTl_Tb_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Sort', 'int', @PrpTl_Sort, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_GridName', 'varchar', @PrpTl_GridName, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Header', 'varchar', @PrpTl_Header, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_IsDefaultView', 'bit', @PrpTl_IsDefaultView, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_IsActiveRow', 'bit', @PrpTl_IsActiveRow, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_UserId', 'uniqueidentifier', @PrpTl_UserId, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 10;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTile_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTile_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTile_putUpdate', 'spWcPropsTile_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTile_putUpdate', 'spWcPropsTile_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTile_putUpdate', 'spWcPropsTile_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTile_putUpdate', 'spWcPropsTile_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTile_putUpdate', 'spWcPropsTile_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTile_putUpdate]
-- Script for this SP was created on: 12/27/2017 4:59:21 PM
(
@PrpTl_Id uniqueidentifier = NULL OUTPUT,
@PrpTl_Tb_Id uniqueidentifier,
@PrpTl_Sort int,
@PrpTl_GridName varchar(50),
@PrpTl_Header varchar(75),
@PrpTl_IsDefaultView bit,
@PrpTl_IsActiveRow bit,
@PrpTl_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcPropsTile'
		UPDATE	wcPropsTile
		SET
		PrpTl_Sort = @PrpTl_Sort,
		PrpTl_GridName = RTRIM(LTRIM(@PrpTl_GridName)),
		PrpTl_Header = RTRIM(LTRIM(@PrpTl_Header)),
		PrpTl_IsDefaultView = @PrpTl_IsDefaultView,
		PrpTl_IsActiveRow = @PrpTl_IsActiveRow,
		PrpTl_UserId = @PrpTl_UserId,
		PrpTl_LastModifiedDate = GETDATE()
		WHERE	(PrpTl_Id=@PrpTl_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Id', 'uniqueidentifier', @PrpTl_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Tb_Id', 'uniqueidentifier', @PrpTl_Tb_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Sort', 'int', @PrpTl_Sort, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_GridName', 'varchar', @PrpTl_GridName, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Header', 'varchar', @PrpTl_Header, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_IsDefaultView', 'bit', @PrpTl_IsDefaultView, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_IsActiveRow', 'bit', @PrpTl_IsActiveRow, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_UserId', 'uniqueidentifier', @PrpTl_UserId, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 10;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTile_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTile_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTile_put', 'spWcPropsTile_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTile_put', 'spWcPropsTile_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTile_put', 'spWcPropsTile_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTile_put', 'spWcPropsTile_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTile_put', 'spWcPropsTile_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTile_put]
-- Script for this SP was created on: 12/27/2017 4:59:21 PM
(
@PrpTl_Id uniqueidentifier = NULL OUTPUT,
@PrpTl_Tb_Id uniqueidentifier,
@PrpTl_Sort int,
@PrpTl_GridName varchar(50),
@PrpTl_Header varchar(75),
@PrpTl_IsDefaultView bit,
@PrpTl_IsActiveRow bit,
@PrpTl_UserId uniqueidentifier,
@PrpTl_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@PrpTl_Id Is NULL)
	BEGIN
		SET @PrpTl_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcPropsTile WHERE PrpTl_Id = @PrpTl_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcPropsTile_putInsert'
		EXEC spWcPropsTile_putInsert
		@PrpTl_Id,
		@PrpTl_Tb_Id,
		@PrpTl_Sort,
		@PrpTl_GridName,
		@PrpTl_Header,
		@PrpTl_IsDefaultView,
		@PrpTl_IsActiveRow,
		@PrpTl_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @PrpTl_Stamp =( SELECT PrpTl_Stamp FROM wcPropsTile WHERE (PrpTl_Id = @PrpTl_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcPropsTile WHERE (PrpTl_Id = @PrpTl_Id) AND (PrpTl_Stamp = @PrpTl_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcPropsTile_putUpdate'
				EXEC spWcPropsTile_putUpdate
				@PrpTl_Id,
				@PrpTl_Tb_Id,
				@PrpTl_Sort,
				@PrpTl_GridName,
				@PrpTl_Header,
				@PrpTl_IsDefaultView,
				@PrpTl_IsActiveRow,
				@PrpTl_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @PrpTl_Stamp =( SELECT PrpTl_Stamp FROM wcPropsTile WHERE (PrpTl_Id = @PrpTl_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Id', 'uniqueidentifier', @PrpTl_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Tb_Id', 'uniqueidentifier', @PrpTl_Tb_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Sort', 'int', @PrpTl_Sort, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_GridName', 'varchar', @PrpTl_GridName, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_Header', 'varchar', @PrpTl_Header, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_IsDefaultView', 'bit', @PrpTl_IsDefaultView, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_IsActiveRow', 'bit', @PrpTl_IsActiveRow, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTl_UserId', 'uniqueidentifier', @PrpTl_UserId, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 10;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTile_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTile_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTile_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTile_putIsDeleted', 'spWcPropsTile_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTile_putIsDeleted', 'spWcPropsTile_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTile_putIsDeleted', 'spWcPropsTile_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTile_putIsDeleted', 'spWcPropsTile_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTile_putIsDeleted', 'spWcPropsTile_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTile_putIsDeleted]
-- Script for this SP was created on: 12/27/2017 4:59:21 PM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcPropsTile
	   SET          PrpTl_IsDeleted = @IsDeleted
	   WHERE        (PrpTl_Id = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN





--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcPropsTile')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcPropsTile', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


