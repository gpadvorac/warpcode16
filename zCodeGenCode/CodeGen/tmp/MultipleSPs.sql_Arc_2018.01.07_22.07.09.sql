-- wcStoredProcParam


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcStoredProcParam

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParam_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParam_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParam_lst', 'spWcStoredProcParam_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParam_lst', 'spWcStoredProcParam_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParam_lst', 'spWcStoredProcParam_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParam_lst', 'spWcStoredProcParam_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParam_lst', 'spWcStoredProcParam_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParam_lst]
-- Script for this SP was created on: 1/7/2018 1:06:32 PM
(
@SpP_Sp_ID uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
SpP_ID,
SpP_Sp_ID,
SpP_SortOrder,
SpP_Name,
SpP_SpPDr_Id,
SpPDr_Name AS SpP_SpPDr_Id_TextField,

SpP_DtNtTp_Id,
DtDtNt_Name AS SpP_DtNtTp_Id_TextField,

SpP_SqlTp_Id,
DtSql_Name AS SpP_SqlTp_Id_TextField,

SpP_IsNullable,
SpP_DefaultValue,
SpP_Value,
SpP_IsActiveRow,
SpP_IsDeleted,
SpP_CreatedUserId,
SpP_CreatedDate,
SpP_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

SpP_LastModifiedDate,
SpP_Stamp


FROM 		wcStoredProcParam LEFT OUTER JOIN
                tbPerson ON wcStoredProcParam.SpP_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcStoredProcParamDirection ON wcStoredProcParam.SpP_SpPDr_Id = wcStoredProcParamDirection.SpPDr_Id LEFT OUTER JOIN
                wcDataTypeDotNet ON wcStoredProcParam.SpP_DtNtTp_Id = wcDataTypeDotNet.DtDtNt_Id LEFT OUTER JOIN
                wcDataTypeSQL ON wcStoredProcParam.SpP_SqlTp_Id = wcDataTypeSQL.DtSql_Id


WHERE   (SpP_Sp_ID = @SpP_Sp_ID) AND (SpP_IsDeleted = 0)

ORDER BY    SpP_SortOrder


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParam_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParam_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParam_row', 'spWcStoredProcParam_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParam_row', 'spWcStoredProcParam_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParam_row', 'spWcStoredProcParam_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParam_row', 'spWcStoredProcParam_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParam_row', 'spWcStoredProcParam_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParam_row]
-- Script for this SP was created on: 1/7/2018 1:06:32 PM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
SpP_ID,
SpP_Sp_ID,
SpP_SortOrder,
SpP_Name,
SpP_SpPDr_Id,
SpPDr_Name AS SpP_SpPDr_Id_TextField,

SpP_DtNtTp_Id,
DtDtNt_Name AS SpP_DtNtTp_Id_TextField,

SpP_SqlTp_Id,
DtSql_Name AS SpP_SqlTp_Id_TextField,

SpP_IsNullable,
SpP_DefaultValue,
SpP_Value,
SpP_IsActiveRow,
SpP_IsDeleted,
SpP_CreatedUserId,
SpP_CreatedDate,
SpP_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

SpP_LastModifiedDate,
SpP_Stamp


FROM 		wcStoredProcParam LEFT OUTER JOIN
                tbPerson ON wcStoredProcParam.SpP_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcStoredProcParamDirection ON wcStoredProcParam.SpP_SpPDr_Id = wcStoredProcParamDirection.SpPDr_Id LEFT OUTER JOIN
                wcDataTypeDotNet ON wcStoredProcParam.SpP_DtNtTp_Id = wcDataTypeDotNet.DtDtNt_Id LEFT OUTER JOIN
                wcDataTypeSQL ON wcStoredProcParam.SpP_SqlTp_Id = wcDataTypeSQL.DtSql_Id


WHERE   (SpP_ID = @Id)

ORDER BY    SpP_SortOrder


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParam_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParam_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParam_lstAll', 'spWcStoredProcParam_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParam_lstAll', 'spWcStoredProcParam_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParam_lstAll', 'spWcStoredProcParam_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParam_lstAll', 'spWcStoredProcParam_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParam_lstAll', 'spWcStoredProcParam_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParam_lstAll]
-- Script for this SP was created on: 1/7/2018 1:06:32 PM
AS
SET NOCOUNT ON

SELECT 	
SpP_ID,
SpP_Sp_ID,
SpP_SortOrder,
SpP_Name,
SpP_SpPDr_Id,
SpPDr_Name AS SpP_SpPDr_Id_TextField,

SpP_DtNtTp_Id,
DtDtNt_Name AS SpP_DtNtTp_Id_TextField,

SpP_SqlTp_Id,
DtSql_Name AS SpP_SqlTp_Id_TextField,

SpP_IsNullable,
SpP_DefaultValue,
SpP_Value,
SpP_IsActiveRow,
SpP_IsDeleted,
SpP_CreatedUserId,
SpP_CreatedDate,
SpP_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

SpP_LastModifiedDate,
SpP_Stamp


FROM 		wcStoredProcParam LEFT OUTER JOIN
                tbPerson ON wcStoredProcParam.SpP_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcStoredProcParamDirection ON wcStoredProcParam.SpP_SpPDr_Id = wcStoredProcParamDirection.SpPDr_Id LEFT OUTER JOIN
                wcDataTypeDotNet ON wcStoredProcParam.SpP_DtNtTp_Id = wcDataTypeDotNet.DtDtNt_Id LEFT OUTER JOIN
                wcDataTypeSQL ON wcStoredProcParam.SpP_SqlTp_Id = wcDataTypeSQL.DtSql_Id

WHERE   (SpP_IsDeleted = 0)

ORDER BY    SpP_SortOrder


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParam_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParam_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParam_putInsert', 'spWcStoredProcParam_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParam_putInsert', 'spWcStoredProcParam_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParam_putInsert', 'spWcStoredProcParam_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParam_putInsert', 'spWcStoredProcParam_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParam_putInsert', 'spWcStoredProcParam_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParam_putInsert]
-- Script for this SP was created on: 1/7/2018 1:06:32 PM
(
@SpP_ID uniqueidentifier = NULL OUTPUT,
@SpP_Sp_ID uniqueidentifier,
@SpP_SortOrder int,
@SpP_Name varchar(50),
@SpP_SpPDr_Id int,
@SpP_DtNtTp_Id int,
@SpP_SqlTp_Id int,
@SpP_IsNullable bit,
@SpP_DefaultValue varchar(100),
@SpP_Value varchar(300),
@SpP_IsActiveRow bit,
@SpP_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcStoredProcParam'
      INSERT INTO wcStoredProcParam
          (
		SpP_ID,
		SpP_Sp_ID,
		SpP_SortOrder,
		SpP_Name,
		SpP_SpPDr_Id,
		SpP_DtNtTp_Id,
		SpP_SqlTp_Id,
		SpP_IsNullable,
		SpP_DefaultValue,
		SpP_Value,
		SpP_IsActiveRow,
		SpP_CreatedUserId,
		SpP_CreatedDate,
		SpP_UserId,
		SpP_LastModifiedDate
				)
VALUES	(
		@SpP_ID, 
		@SpP_Sp_ID, 
		@SpP_SortOrder, 
		RTRIM(LTRIM(@SpP_Name)), 
		@SpP_SpPDr_Id, 
		@SpP_DtNtTp_Id, 
		@SpP_SqlTp_Id, 
		@SpP_IsNullable, 
		RTRIM(LTRIM(@SpP_DefaultValue)), 
		RTRIM(LTRIM(@SpP_Value)), 
		@SpP_IsActiveRow, 
		@SpP_UserId, 
		GETDATE(), 
		@SpP_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_ID', 'uniqueidentifier', @SpP_ID, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_Sp_ID', 'uniqueidentifier', @SpP_Sp_ID, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_SortOrder', 'int', @SpP_SortOrder, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_Name', 'varchar', @SpP_Name, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_SpPDr_Id', 'int', @SpP_SpPDr_Id, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_DtNtTp_Id', 'int', @SpP_DtNtTp_Id, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_SqlTp_Id', 'int', @SpP_SqlTp_Id, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_IsNullable', 'bit', @SpP_IsNullable, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_DefaultValue', 'varchar', @SpP_DefaultValue, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_Value', 'varchar', @SpP_Value, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_IsActiveRow', 'bit', @SpP_IsActiveRow, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_UserId', 'uniqueidentifier', @SpP_UserId, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 14;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParam_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParam_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParam_putUpdate', 'spWcStoredProcParam_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParam_putUpdate', 'spWcStoredProcParam_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParam_putUpdate', 'spWcStoredProcParam_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParam_putUpdate', 'spWcStoredProcParam_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParam_putUpdate', 'spWcStoredProcParam_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParam_putUpdate]
-- Script for this SP was created on: 1/7/2018 1:06:32 PM
(
@SpP_ID uniqueidentifier = NULL OUTPUT,
@SpP_Sp_ID uniqueidentifier,
@SpP_SortOrder int,
@SpP_Name varchar(50),
@SpP_SpPDr_Id int,
@SpP_DtNtTp_Id int,
@SpP_SqlTp_Id int,
@SpP_IsNullable bit,
@SpP_DefaultValue varchar(100),
@SpP_Value varchar(300),
@SpP_IsActiveRow bit,
@SpP_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcStoredProcParam'
		UPDATE	wcStoredProcParam
		SET
		SpP_SortOrder = @SpP_SortOrder,
		SpP_Name = RTRIM(LTRIM(@SpP_Name)),
		SpP_SpPDr_Id = @SpP_SpPDr_Id,
		SpP_DtNtTp_Id = @SpP_DtNtTp_Id,
		SpP_SqlTp_Id = @SpP_SqlTp_Id,
		SpP_IsNullable = @SpP_IsNullable,
		SpP_DefaultValue = RTRIM(LTRIM(@SpP_DefaultValue)),
		SpP_Value = RTRIM(LTRIM(@SpP_Value)),
		SpP_IsActiveRow = @SpP_IsActiveRow,
		SpP_UserId = @SpP_UserId,
		SpP_LastModifiedDate = GETDATE()
		WHERE	(SpP_ID=@SpP_ID)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_ID', 'uniqueidentifier', @SpP_ID, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_Sp_ID', 'uniqueidentifier', @SpP_Sp_ID, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_SortOrder', 'int', @SpP_SortOrder, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_Name', 'varchar', @SpP_Name, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_SpPDr_Id', 'int', @SpP_SpPDr_Id, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_DtNtTp_Id', 'int', @SpP_DtNtTp_Id, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_SqlTp_Id', 'int', @SpP_SqlTp_Id, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_IsNullable', 'bit', @SpP_IsNullable, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_DefaultValue', 'varchar', @SpP_DefaultValue, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_Value', 'varchar', @SpP_Value, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_IsActiveRow', 'bit', @SpP_IsActiveRow, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_UserId', 'uniqueidentifier', @SpP_UserId, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 14;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParam_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParam_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParam_put', 'spWcStoredProcParam_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParam_put', 'spWcStoredProcParam_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParam_put', 'spWcStoredProcParam_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParam_put', 'spWcStoredProcParam_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParam_put', 'spWcStoredProcParam_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParam_put]
-- Script for this SP was created on: 1/7/2018 1:06:32 PM
(
@SpP_ID uniqueidentifier = NULL OUTPUT,
@SpP_Sp_ID uniqueidentifier,
@SpP_SortOrder int,
@SpP_Name varchar(50),
@SpP_SpPDr_Id int,
@SpP_DtNtTp_Id int,
@SpP_SqlTp_Id int,
@SpP_IsNullable bit,
@SpP_DefaultValue varchar(100),
@SpP_Value varchar(300),
@SpP_IsActiveRow bit,
@SpP_UserId uniqueidentifier,
@SpP_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@SpP_ID Is NULL)
	BEGIN
		SET @SpP_ID = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcStoredProcParam WHERE SpP_ID = @SpP_ID)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcStoredProcParam_putInsert'
		EXEC spWcStoredProcParam_putInsert
		@SpP_ID,
		@SpP_Sp_ID,
		@SpP_SortOrder,
		@SpP_Name,
		@SpP_SpPDr_Id,
		@SpP_DtNtTp_Id,
		@SpP_SqlTp_Id,
		@SpP_IsNullable,
		@SpP_DefaultValue,
		@SpP_Value,
		@SpP_IsActiveRow,
		@SpP_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @SpP_Stamp =( SELECT SpP_Stamp FROM wcStoredProcParam WHERE (SpP_ID = @SpP_ID))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcStoredProcParam WHERE (SpP_ID = @SpP_ID) AND (SpP_Stamp = @SpP_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcStoredProcParam_putUpdate'
				EXEC spWcStoredProcParam_putUpdate
				@SpP_ID,
				@SpP_Sp_ID,
				@SpP_SortOrder,
				@SpP_Name,
				@SpP_SpPDr_Id,
				@SpP_DtNtTp_Id,
				@SpP_SqlTp_Id,
				@SpP_IsNullable,
				@SpP_DefaultValue,
				@SpP_Value,
				@SpP_IsActiveRow,
				@SpP_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @SpP_Stamp =( SELECT SpP_Stamp FROM wcStoredProcParam WHERE (SpP_ID = @SpP_ID))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_ID', 'uniqueidentifier', @SpP_ID, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_Sp_ID', 'uniqueidentifier', @SpP_Sp_ID, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_SortOrder', 'int', @SpP_SortOrder, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_Name', 'varchar', @SpP_Name, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_SpPDr_Id', 'int', @SpP_SpPDr_Id, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_DtNtTp_Id', 'int', @SpP_DtNtTp_Id, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_SqlTp_Id', 'int', @SpP_SqlTp_Id, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_IsNullable', 'bit', @SpP_IsNullable, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_DefaultValue', 'varchar', @SpP_DefaultValue, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_Value', 'varchar', @SpP_Value, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_IsActiveRow', 'bit', @SpP_IsActiveRow, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpP_UserId', 'uniqueidentifier', @SpP_UserId, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 14;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParam_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParam_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParam_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParam_putIsDeleted', 'spWcStoredProcParam_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParam_putIsDeleted', 'spWcStoredProcParam_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParam_putIsDeleted', 'spWcStoredProcParam_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParam_putIsDeleted', 'spWcStoredProcParam_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParam_putIsDeleted', 'spWcStoredProcParam_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParam_putIsDeleted]
-- Script for this SP was created on: 1/7/2018 1:06:32 PM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcStoredProcParam
	   SET          SpP_IsDeleted = @IsDeleted
	   WHERE        (SpP_ID = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN





--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcStoredProcParam')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcStoredProcParam', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


