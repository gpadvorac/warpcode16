-- wcTable


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcTable

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTable_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTable_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTable_lst', 'spWcTable_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTable_lst', 'spWcTable_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTable_lst', 'spWcTable_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTable_lst', 'spWcTable_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTable_lst', 'spWcTable_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTable_lst]
-- Script for this SP was created on: 1/7/2018 10:07:08 PM
(
@Tb_ApVrsn_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
Tb_Id,
Tb_ApVrsn_Id,
Tb_auditTable_Id,
AttachmentCount,
AttachmentFileNames,
DiscussionCount,
DiscussionTitles,
Tb_Name,
Tb_EntityRootName,
Tb_VariableName,
Tb_ScreenCaption,
Tb_Description,
Tb_DevelopmentNote,
Tb_UseLegacyConnectionCode,
Tb_DbCnSK_Id,
DbCnSK_Name AS Tb_DbCnSK_Id_TextField,

Tb_ApDbScm_Id,
ApDbScm_Ap_Id AS Tb_ApDbScm_Id_TextField,

Tb_UIAssemblyName,
Tb_UINamespace,
Tb_UIAssemblyPath,
Tb_ProxyAssemblyName,
Tb_ProxyNamespace,
Tb_ProxyAssemblyPath,
Tb_WireTypeAssemblyName,
Tb_WireTypeNamespace,
Tb_WireTypePath,
Tb_WebServiceName,
Tb_WebServiceFolder,
Tb_DataServiceAssemblyName,
Tb_DataServiceNamespace,
Tb_DataServicePath,
Tb_UseTilesInPropsScreen,
Tb_UseGridColumnGroups,
Tb_UseGridDataSourceCombo,
Tb_PkIsIdentity,
Tb_IsVirtual,
Tb_IsScreenPlaceHolder,
Tb_IsNotEntity,
Tb_IsMany2Many,
Tb_UseLastModifiedByUserNameInSproc,
Tb_UseUserTimeStamp,
Tb_UseForAudit,
Tb_IsAllowDelete,
Tb_CnfgGdMnu_MenuRow_IsVisible,
Tb_CnfgGdMnu_GridTools_IsVisible,
Tb_CnfgGdMnu_SplitScreen_IsVisible,
Tb_CnfgGdMnu_SplitScreen_IsSplit_Default,
Tb_CnfgGdMnu_NavColumnWidth,
Tb_CnfgGdMnu_IsReadOnly,
Tb_CnfgGdMnu_IsAllowNewRow,
Tb_CnfgGdMnu_ExcelExport_IsVisible,
Tb_CnfgGdMnu_ColumnChooser_IsVisible,
Tb_CnfgGdMnu_ShowHideColBtn_IsVisible,
Tb_CnfgGdMnu_RefreshGrid_IsVisible,
Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible,
Tb_IsInputComplete,
Tb_IsCodeGen,
Tb_IsReadyCodeGen,
Tb_IsCodeGenComplete,
Tb_IsTagForCodeGen,
Tb_IsTagForOther,
Tb_TableGroups,
Tb_IsActiveRow,
Tb_IsDeleted,
Tb_CreatedUserId,
Tb_CreatedDate,
Tb_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

Tb_LastModifiedDate,
Tb_Stamp


FROM 		wcTable LEFT OUTER JOIN
                tbPerson ON wcTable.Tb_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcDatabaseConnectionStringKey ON wcTable.Tb_DbCnSK_Id = wcDatabaseConnectionStringKey.DbCnSK_Id LEFT OUTER JOIN
                wcAppDbSchema ON wcTable.Tb_ApDbScm_Id = wcAppDbSchema.ApDbScm_Id


WHERE   (Tb_ApVrsn_Id = @Tb_ApVrsn_Id) AND (Tb_IsDeleted = 0)

ORDER BY    Tb_Name


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTable_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTable_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTable_putInsert', 'spWcTable_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTable_putInsert', 'spWcTable_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTable_putInsert', 'spWcTable_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTable_putInsert', 'spWcTable_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTable_putInsert', 'spWcTable_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTable_putInsert]
-- Script for this SP was created on: 1/7/2018 10:07:08 PM
(
@Tb_Id uniqueidentifier = NULL OUTPUT,
@Tb_ApVrsn_Id uniqueidentifier,
@Tb_Name varchar(100),
@Tb_EntityRootName varchar(75),
@Tb_VariableName varchar(100),
@Tb_ScreenCaption varchar(100),
@Tb_Description varchar(2000),
@Tb_DevelopmentNote varchar(2000),
@Tb_UseLegacyConnectionCode bit,
@Tb_DbCnSK_Id uniqueidentifier,
@Tb_ApDbScm_Id uniqueidentifier,
@Tb_UIAssemblyName varchar(75),
@Tb_UINamespace varchar(75),
@Tb_UIAssemblyPath varchar(250),
@Tb_ProxyAssemblyName varchar(75),
@Tb_ProxyNamespace varchar(100),
@Tb_ProxyAssemblyPath varchar(250),
@Tb_WireTypeAssemblyName varchar(100),
@Tb_WireTypeNamespace varchar(100),
@Tb_WireTypePath varchar(250),
@Tb_WebServiceName varchar(100),
@Tb_WebServiceFolder varchar(100),
@Tb_DataServiceAssemblyName varchar(100),
@Tb_DataServiceNamespace varchar(100),
@Tb_DataServicePath varchar(250),
@Tb_UseTilesInPropsScreen bit,
@Tb_UseGridColumnGroups bit,
@Tb_UseGridDataSourceCombo bit,
@Tb_PkIsIdentity bit,
@Tb_IsVirtual bit,
@Tb_IsScreenPlaceHolder bit,
@Tb_IsNotEntity bit,
@Tb_IsMany2Many bit,
@Tb_UseLastModifiedByUserNameInSproc bit,
@Tb_UseUserTimeStamp bit,
@Tb_UseForAudit bit,
@Tb_IsAllowDelete bit,
@Tb_CnfgGdMnu_MenuRow_IsVisible bit,
@Tb_CnfgGdMnu_GridTools_IsVisible bit,
@Tb_CnfgGdMnu_SplitScreen_IsVisible bit,
@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default bit,
@Tb_CnfgGdMnu_NavColumnWidth int,
@Tb_CnfgGdMnu_IsReadOnly bit,
@Tb_CnfgGdMnu_IsAllowNewRow bit,
@Tb_CnfgGdMnu_ExcelExport_IsVisible bit,
@Tb_CnfgGdMnu_ColumnChooser_IsVisible bit,
@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible bit,
@Tb_CnfgGdMnu_RefreshGrid_IsVisible bit,
@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible bit,
@Tb_IsInputComplete bit,
@Tb_IsCodeGen bit,
@Tb_IsReadyCodeGen bit,
@Tb_IsCodeGenComplete bit,
@Tb_IsTagForCodeGen bit,
@Tb_IsTagForOther bit,
@Tb_IsActiveRow bit,
@Tb_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcTable'
      INSERT INTO wcTable
          (
		Tb_Id,
		Tb_ApVrsn_Id,
		Tb_Name,
		Tb_EntityRootName,
		Tb_VariableName,
		Tb_ScreenCaption,
		Tb_Description,
		Tb_DevelopmentNote,
		Tb_UseLegacyConnectionCode,
		Tb_DbCnSK_Id,
		Tb_ApDbScm_Id,
		Tb_UIAssemblyName,
		Tb_UINamespace,
		Tb_UIAssemblyPath,
		Tb_ProxyAssemblyName,
		Tb_ProxyNamespace,
		Tb_ProxyAssemblyPath,
		Tb_WireTypeAssemblyName,
		Tb_WireTypeNamespace,
		Tb_WireTypePath,
		Tb_WebServiceName,
		Tb_WebServiceFolder,
		Tb_DataServiceAssemblyName,
		Tb_DataServiceNamespace,
		Tb_DataServicePath,
		Tb_UseTilesInPropsScreen,
		Tb_UseGridColumnGroups,
		Tb_UseGridDataSourceCombo,
		Tb_PkIsIdentity,
		Tb_IsVirtual,
		Tb_IsScreenPlaceHolder,
		Tb_IsNotEntity,
		Tb_IsMany2Many,
		Tb_UseLastModifiedByUserNameInSproc,
		Tb_UseUserTimeStamp,
		Tb_UseForAudit,
		Tb_IsAllowDelete,
		Tb_CnfgGdMnu_MenuRow_IsVisible,
		Tb_CnfgGdMnu_GridTools_IsVisible,
		Tb_CnfgGdMnu_SplitScreen_IsVisible,
		Tb_CnfgGdMnu_SplitScreen_IsSplit_Default,
		Tb_CnfgGdMnu_NavColumnWidth,
		Tb_CnfgGdMnu_IsReadOnly,
		Tb_CnfgGdMnu_IsAllowNewRow,
		Tb_CnfgGdMnu_ExcelExport_IsVisible,
		Tb_CnfgGdMnu_ColumnChooser_IsVisible,
		Tb_CnfgGdMnu_ShowHideColBtn_IsVisible,
		Tb_CnfgGdMnu_RefreshGrid_IsVisible,
		Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible,
		Tb_IsInputComplete,
		Tb_IsCodeGen,
		Tb_IsReadyCodeGen,
		Tb_IsCodeGenComplete,
		Tb_IsTagForCodeGen,
		Tb_IsTagForOther,
		Tb_IsActiveRow,
		Tb_CreatedUserId,
		Tb_CreatedDate,
		Tb_UserId,
		Tb_LastModifiedDate
				)
VALUES	(
		@Tb_Id, 
		@Tb_ApVrsn_Id, 
		RTRIM(LTRIM(@Tb_Name)), 
		RTRIM(LTRIM(@Tb_EntityRootName)), 
		RTRIM(LTRIM(@Tb_VariableName)), 
		RTRIM(LTRIM(@Tb_ScreenCaption)), 
		RTRIM(LTRIM(@Tb_Description)), 
		RTRIM(LTRIM(@Tb_DevelopmentNote)), 
		@Tb_UseLegacyConnectionCode, 
		@Tb_DbCnSK_Id, 
		@Tb_ApDbScm_Id, 
		RTRIM(LTRIM(@Tb_UIAssemblyName)), 
		RTRIM(LTRIM(@Tb_UINamespace)), 
		RTRIM(LTRIM(@Tb_UIAssemblyPath)), 
		RTRIM(LTRIM(@Tb_ProxyAssemblyName)), 
		RTRIM(LTRIM(@Tb_ProxyNamespace)), 
		RTRIM(LTRIM(@Tb_ProxyAssemblyPath)), 
		RTRIM(LTRIM(@Tb_WireTypeAssemblyName)), 
		RTRIM(LTRIM(@Tb_WireTypeNamespace)), 
		RTRIM(LTRIM(@Tb_WireTypePath)), 
		RTRIM(LTRIM(@Tb_WebServiceName)), 
		RTRIM(LTRIM(@Tb_WebServiceFolder)), 
		RTRIM(LTRIM(@Tb_DataServiceAssemblyName)), 
		RTRIM(LTRIM(@Tb_DataServiceNamespace)), 
		RTRIM(LTRIM(@Tb_DataServicePath)), 
		@Tb_UseTilesInPropsScreen, 
		@Tb_UseGridColumnGroups, 
		@Tb_UseGridDataSourceCombo, 
		@Tb_PkIsIdentity, 
		@Tb_IsVirtual, 
		@Tb_IsScreenPlaceHolder, 
		@Tb_IsNotEntity, 
		@Tb_IsMany2Many, 
		@Tb_UseLastModifiedByUserNameInSproc, 
		@Tb_UseUserTimeStamp, 
		@Tb_UseForAudit, 
		@Tb_IsAllowDelete, 
		@Tb_CnfgGdMnu_MenuRow_IsVisible, 
		@Tb_CnfgGdMnu_GridTools_IsVisible, 
		@Tb_CnfgGdMnu_SplitScreen_IsVisible, 
		@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default, 
		@Tb_CnfgGdMnu_NavColumnWidth, 
		@Tb_CnfgGdMnu_IsReadOnly, 
		@Tb_CnfgGdMnu_IsAllowNewRow, 
		@Tb_CnfgGdMnu_ExcelExport_IsVisible, 
		@Tb_CnfgGdMnu_ColumnChooser_IsVisible, 
		@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible, 
		@Tb_CnfgGdMnu_RefreshGrid_IsVisible, 
		@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible, 
		@Tb_IsInputComplete, 
		@Tb_IsCodeGen, 
		@Tb_IsReadyCodeGen, 
		@Tb_IsCodeGenComplete, 
		@Tb_IsTagForCodeGen, 
		@Tb_IsTagForOther, 
		@Tb_IsActiveRow, 
		@Tb_UserId, 
		GETDATE(), 
		@Tb_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_Id', 'uniqueidentifier', @Tb_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ApVrsn_Id', 'uniqueidentifier', @Tb_ApVrsn_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_Name', 'varchar', @Tb_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_EntityRootName', 'varchar', @Tb_EntityRootName, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_VariableName', 'varchar', @Tb_VariableName, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ScreenCaption', 'varchar', @Tb_ScreenCaption, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_Description', 'varchar', @Tb_Description, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DevelopmentNote', 'varchar', @Tb_DevelopmentNote, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseLegacyConnectionCode', 'bit', @Tb_UseLegacyConnectionCode, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DbCnSK_Id', 'uniqueidentifier', @Tb_DbCnSK_Id, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ApDbScm_Id', 'uniqueidentifier', @Tb_ApDbScm_Id, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UIAssemblyName', 'varchar', @Tb_UIAssemblyName, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UINamespace', 'varchar', @Tb_UINamespace, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UIAssemblyPath', 'varchar', @Tb_UIAssemblyPath, 14;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ProxyAssemblyName', 'varchar', @Tb_ProxyAssemblyName, 15;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ProxyNamespace', 'varchar', @Tb_ProxyNamespace, 16;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ProxyAssemblyPath', 'varchar', @Tb_ProxyAssemblyPath, 17;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WireTypeAssemblyName', 'varchar', @Tb_WireTypeAssemblyName, 18;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WireTypeNamespace', 'varchar', @Tb_WireTypeNamespace, 19;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WireTypePath', 'varchar', @Tb_WireTypePath, 20;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WebServiceName', 'varchar', @Tb_WebServiceName, 21;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WebServiceFolder', 'varchar', @Tb_WebServiceFolder, 22;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DataServiceAssemblyName', 'varchar', @Tb_DataServiceAssemblyName, 23;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DataServiceNamespace', 'varchar', @Tb_DataServiceNamespace, 24;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DataServicePath', 'varchar', @Tb_DataServicePath, 25;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseTilesInPropsScreen', 'bit', @Tb_UseTilesInPropsScreen, 26;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseGridColumnGroups', 'bit', @Tb_UseGridColumnGroups, 27;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseGridDataSourceCombo', 'bit', @Tb_UseGridDataSourceCombo, 28;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_PkIsIdentity', 'bit', @Tb_PkIsIdentity, 29;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsVirtual', 'bit', @Tb_IsVirtual, 30;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsScreenPlaceHolder', 'bit', @Tb_IsScreenPlaceHolder, 31;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsNotEntity', 'bit', @Tb_IsNotEntity, 32;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsMany2Many', 'bit', @Tb_IsMany2Many, 33;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseLastModifiedByUserNameInSproc', 'bit', @Tb_UseLastModifiedByUserNameInSproc, 34;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseUserTimeStamp', 'bit', @Tb_UseUserTimeStamp, 35;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseForAudit', 'bit', @Tb_UseForAudit, 36;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsAllowDelete', 'bit', @Tb_IsAllowDelete, 37;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_MenuRow_IsVisible', 'bit', @Tb_CnfgGdMnu_MenuRow_IsVisible, 38;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_GridTools_IsVisible', 'bit', @Tb_CnfgGdMnu_GridTools_IsVisible, 39;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_SplitScreen_IsVisible', 'bit', @Tb_CnfgGdMnu_SplitScreen_IsVisible, 40;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default', 'bit', @Tb_CnfgGdMnu_SplitScreen_IsSplit_Default, 41;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_NavColumnWidth', 'int', @Tb_CnfgGdMnu_NavColumnWidth, 42;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_IsReadOnly', 'bit', @Tb_CnfgGdMnu_IsReadOnly, 43;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_IsAllowNewRow', 'bit', @Tb_CnfgGdMnu_IsAllowNewRow, 44;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_ExcelExport_IsVisible', 'bit', @Tb_CnfgGdMnu_ExcelExport_IsVisible, 45;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_ColumnChooser_IsVisible', 'bit', @Tb_CnfgGdMnu_ColumnChooser_IsVisible, 46;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible', 'bit', @Tb_CnfgGdMnu_ShowHideColBtn_IsVisible, 47;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_RefreshGrid_IsVisible', 'bit', @Tb_CnfgGdMnu_RefreshGrid_IsVisible, 48;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible', 'bit', @Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible, 49;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsInputComplete', 'bit', @Tb_IsInputComplete, 50;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsCodeGen', 'bit', @Tb_IsCodeGen, 51;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsReadyCodeGen', 'bit', @Tb_IsReadyCodeGen, 52;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsCodeGenComplete', 'bit', @Tb_IsCodeGenComplete, 53;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsTagForCodeGen', 'bit', @Tb_IsTagForCodeGen, 54;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsTagForOther', 'bit', @Tb_IsTagForOther, 55;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsActiveRow', 'bit', @Tb_IsActiveRow, 56;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UserId', 'uniqueidentifier', @Tb_UserId, 57;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 58;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 59;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTable_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTable_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTable_putUpdate', 'spWcTable_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTable_putUpdate', 'spWcTable_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTable_putUpdate', 'spWcTable_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTable_putUpdate', 'spWcTable_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTable_putUpdate', 'spWcTable_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTable_putUpdate]
-- Script for this SP was created on: 1/7/2018 10:07:08 PM
(
@Tb_Id uniqueidentifier = NULL OUTPUT,
@Tb_ApVrsn_Id uniqueidentifier,
@Tb_Name varchar(100),
@Tb_EntityRootName varchar(75),
@Tb_VariableName varchar(100),
@Tb_ScreenCaption varchar(100),
@Tb_Description varchar(2000),
@Tb_DevelopmentNote varchar(2000),
@Tb_UseLegacyConnectionCode bit,
@Tb_DbCnSK_Id uniqueidentifier,
@Tb_ApDbScm_Id uniqueidentifier,
@Tb_UIAssemblyName varchar(75),
@Tb_UINamespace varchar(75),
@Tb_UIAssemblyPath varchar(250),
@Tb_ProxyAssemblyName varchar(75),
@Tb_ProxyNamespace varchar(100),
@Tb_ProxyAssemblyPath varchar(250),
@Tb_WireTypeAssemblyName varchar(100),
@Tb_WireTypeNamespace varchar(100),
@Tb_WireTypePath varchar(250),
@Tb_WebServiceName varchar(100),
@Tb_WebServiceFolder varchar(100),
@Tb_DataServiceAssemblyName varchar(100),
@Tb_DataServiceNamespace varchar(100),
@Tb_DataServicePath varchar(250),
@Tb_UseTilesInPropsScreen bit,
@Tb_UseGridColumnGroups bit,
@Tb_UseGridDataSourceCombo bit,
@Tb_PkIsIdentity bit,
@Tb_IsVirtual bit,
@Tb_IsScreenPlaceHolder bit,
@Tb_IsNotEntity bit,
@Tb_IsMany2Many bit,
@Tb_UseLastModifiedByUserNameInSproc bit,
@Tb_UseUserTimeStamp bit,
@Tb_UseForAudit bit,
@Tb_IsAllowDelete bit,
@Tb_CnfgGdMnu_MenuRow_IsVisible bit,
@Tb_CnfgGdMnu_GridTools_IsVisible bit,
@Tb_CnfgGdMnu_SplitScreen_IsVisible bit,
@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default bit,
@Tb_CnfgGdMnu_NavColumnWidth int,
@Tb_CnfgGdMnu_IsReadOnly bit,
@Tb_CnfgGdMnu_IsAllowNewRow bit,
@Tb_CnfgGdMnu_ExcelExport_IsVisible bit,
@Tb_CnfgGdMnu_ColumnChooser_IsVisible bit,
@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible bit,
@Tb_CnfgGdMnu_RefreshGrid_IsVisible bit,
@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible bit,
@Tb_IsInputComplete bit,
@Tb_IsCodeGen bit,
@Tb_IsReadyCodeGen bit,
@Tb_IsCodeGenComplete bit,
@Tb_IsTagForCodeGen bit,
@Tb_IsTagForOther bit,
@Tb_IsActiveRow bit,
@Tb_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcTable'
		UPDATE	wcTable
		SET
		Tb_Name = RTRIM(LTRIM(@Tb_Name)),
		Tb_EntityRootName = RTRIM(LTRIM(@Tb_EntityRootName)),
		Tb_VariableName = RTRIM(LTRIM(@Tb_VariableName)),
		Tb_ScreenCaption = RTRIM(LTRIM(@Tb_ScreenCaption)),
		Tb_Description = RTRIM(LTRIM(@Tb_Description)),
		Tb_DevelopmentNote = RTRIM(LTRIM(@Tb_DevelopmentNote)),
		Tb_UseLegacyConnectionCode = @Tb_UseLegacyConnectionCode,
		Tb_DbCnSK_Id = @Tb_DbCnSK_Id,
		Tb_ApDbScm_Id = @Tb_ApDbScm_Id,
		Tb_UIAssemblyName = RTRIM(LTRIM(@Tb_UIAssemblyName)),
		Tb_UINamespace = RTRIM(LTRIM(@Tb_UINamespace)),
		Tb_UIAssemblyPath = RTRIM(LTRIM(@Tb_UIAssemblyPath)),
		Tb_ProxyAssemblyName = RTRIM(LTRIM(@Tb_ProxyAssemblyName)),
		Tb_ProxyNamespace = RTRIM(LTRIM(@Tb_ProxyNamespace)),
		Tb_ProxyAssemblyPath = RTRIM(LTRIM(@Tb_ProxyAssemblyPath)),
		Tb_WireTypeAssemblyName = RTRIM(LTRIM(@Tb_WireTypeAssemblyName)),
		Tb_WireTypeNamespace = RTRIM(LTRIM(@Tb_WireTypeNamespace)),
		Tb_WireTypePath = RTRIM(LTRIM(@Tb_WireTypePath)),
		Tb_WebServiceName = RTRIM(LTRIM(@Tb_WebServiceName)),
		Tb_WebServiceFolder = RTRIM(LTRIM(@Tb_WebServiceFolder)),
		Tb_DataServiceAssemblyName = RTRIM(LTRIM(@Tb_DataServiceAssemblyName)),
		Tb_DataServiceNamespace = RTRIM(LTRIM(@Tb_DataServiceNamespace)),
		Tb_DataServicePath = RTRIM(LTRIM(@Tb_DataServicePath)),
		Tb_UseTilesInPropsScreen = @Tb_UseTilesInPropsScreen,
		Tb_UseGridColumnGroups = @Tb_UseGridColumnGroups,
		Tb_UseGridDataSourceCombo = @Tb_UseGridDataSourceCombo,
		Tb_PkIsIdentity = @Tb_PkIsIdentity,
		Tb_IsVirtual = @Tb_IsVirtual,
		Tb_IsScreenPlaceHolder = @Tb_IsScreenPlaceHolder,
		Tb_IsNotEntity = @Tb_IsNotEntity,
		Tb_IsMany2Many = @Tb_IsMany2Many,
		Tb_UseLastModifiedByUserNameInSproc = @Tb_UseLastModifiedByUserNameInSproc,
		Tb_UseUserTimeStamp = @Tb_UseUserTimeStamp,
		Tb_UseForAudit = @Tb_UseForAudit,
		Tb_IsAllowDelete = @Tb_IsAllowDelete,
		Tb_CnfgGdMnu_MenuRow_IsVisible = @Tb_CnfgGdMnu_MenuRow_IsVisible,
		Tb_CnfgGdMnu_GridTools_IsVisible = @Tb_CnfgGdMnu_GridTools_IsVisible,
		Tb_CnfgGdMnu_SplitScreen_IsVisible = @Tb_CnfgGdMnu_SplitScreen_IsVisible,
		Tb_CnfgGdMnu_SplitScreen_IsSplit_Default = @Tb_CnfgGdMnu_SplitScreen_IsSplit_Default,
		Tb_CnfgGdMnu_NavColumnWidth = @Tb_CnfgGdMnu_NavColumnWidth,
		Tb_CnfgGdMnu_IsReadOnly = @Tb_CnfgGdMnu_IsReadOnly,
		Tb_CnfgGdMnu_IsAllowNewRow = @Tb_CnfgGdMnu_IsAllowNewRow,
		Tb_CnfgGdMnu_ExcelExport_IsVisible = @Tb_CnfgGdMnu_ExcelExport_IsVisible,
		Tb_CnfgGdMnu_ColumnChooser_IsVisible = @Tb_CnfgGdMnu_ColumnChooser_IsVisible,
		Tb_CnfgGdMnu_ShowHideColBtn_IsVisible = @Tb_CnfgGdMnu_ShowHideColBtn_IsVisible,
		Tb_CnfgGdMnu_RefreshGrid_IsVisible = @Tb_CnfgGdMnu_RefreshGrid_IsVisible,
		Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible = @Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible,
		Tb_IsInputComplete = @Tb_IsInputComplete,
		Tb_IsCodeGen = @Tb_IsCodeGen,
		Tb_IsReadyCodeGen = @Tb_IsReadyCodeGen,
		Tb_IsCodeGenComplete = @Tb_IsCodeGenComplete,
		Tb_IsTagForCodeGen = @Tb_IsTagForCodeGen,
		Tb_IsTagForOther = @Tb_IsTagForOther,
		Tb_IsActiveRow = @Tb_IsActiveRow,
		Tb_UserId = @Tb_UserId,
		Tb_LastModifiedDate = GETDATE()
		WHERE	(Tb_Id=@Tb_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_Id', 'uniqueidentifier', @Tb_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ApVrsn_Id', 'uniqueidentifier', @Tb_ApVrsn_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_Name', 'varchar', @Tb_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_EntityRootName', 'varchar', @Tb_EntityRootName, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_VariableName', 'varchar', @Tb_VariableName, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ScreenCaption', 'varchar', @Tb_ScreenCaption, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_Description', 'varchar', @Tb_Description, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DevelopmentNote', 'varchar', @Tb_DevelopmentNote, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseLegacyConnectionCode', 'bit', @Tb_UseLegacyConnectionCode, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DbCnSK_Id', 'uniqueidentifier', @Tb_DbCnSK_Id, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ApDbScm_Id', 'uniqueidentifier', @Tb_ApDbScm_Id, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UIAssemblyName', 'varchar', @Tb_UIAssemblyName, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UINamespace', 'varchar', @Tb_UINamespace, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UIAssemblyPath', 'varchar', @Tb_UIAssemblyPath, 14;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ProxyAssemblyName', 'varchar', @Tb_ProxyAssemblyName, 15;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ProxyNamespace', 'varchar', @Tb_ProxyNamespace, 16;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ProxyAssemblyPath', 'varchar', @Tb_ProxyAssemblyPath, 17;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WireTypeAssemblyName', 'varchar', @Tb_WireTypeAssemblyName, 18;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WireTypeNamespace', 'varchar', @Tb_WireTypeNamespace, 19;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WireTypePath', 'varchar', @Tb_WireTypePath, 20;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WebServiceName', 'varchar', @Tb_WebServiceName, 21;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WebServiceFolder', 'varchar', @Tb_WebServiceFolder, 22;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DataServiceAssemblyName', 'varchar', @Tb_DataServiceAssemblyName, 23;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DataServiceNamespace', 'varchar', @Tb_DataServiceNamespace, 24;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DataServicePath', 'varchar', @Tb_DataServicePath, 25;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseTilesInPropsScreen', 'bit', @Tb_UseTilesInPropsScreen, 26;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseGridColumnGroups', 'bit', @Tb_UseGridColumnGroups, 27;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseGridDataSourceCombo', 'bit', @Tb_UseGridDataSourceCombo, 28;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_PkIsIdentity', 'bit', @Tb_PkIsIdentity, 29;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsVirtual', 'bit', @Tb_IsVirtual, 30;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsScreenPlaceHolder', 'bit', @Tb_IsScreenPlaceHolder, 31;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsNotEntity', 'bit', @Tb_IsNotEntity, 32;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsMany2Many', 'bit', @Tb_IsMany2Many, 33;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseLastModifiedByUserNameInSproc', 'bit', @Tb_UseLastModifiedByUserNameInSproc, 34;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseUserTimeStamp', 'bit', @Tb_UseUserTimeStamp, 35;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseForAudit', 'bit', @Tb_UseForAudit, 36;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsAllowDelete', 'bit', @Tb_IsAllowDelete, 37;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_MenuRow_IsVisible', 'bit', @Tb_CnfgGdMnu_MenuRow_IsVisible, 38;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_GridTools_IsVisible', 'bit', @Tb_CnfgGdMnu_GridTools_IsVisible, 39;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_SplitScreen_IsVisible', 'bit', @Tb_CnfgGdMnu_SplitScreen_IsVisible, 40;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default', 'bit', @Tb_CnfgGdMnu_SplitScreen_IsSplit_Default, 41;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_NavColumnWidth', 'int', @Tb_CnfgGdMnu_NavColumnWidth, 42;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_IsReadOnly', 'bit', @Tb_CnfgGdMnu_IsReadOnly, 43;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_IsAllowNewRow', 'bit', @Tb_CnfgGdMnu_IsAllowNewRow, 44;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_ExcelExport_IsVisible', 'bit', @Tb_CnfgGdMnu_ExcelExport_IsVisible, 45;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_ColumnChooser_IsVisible', 'bit', @Tb_CnfgGdMnu_ColumnChooser_IsVisible, 46;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible', 'bit', @Tb_CnfgGdMnu_ShowHideColBtn_IsVisible, 47;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_RefreshGrid_IsVisible', 'bit', @Tb_CnfgGdMnu_RefreshGrid_IsVisible, 48;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible', 'bit', @Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible, 49;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsInputComplete', 'bit', @Tb_IsInputComplete, 50;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsCodeGen', 'bit', @Tb_IsCodeGen, 51;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsReadyCodeGen', 'bit', @Tb_IsReadyCodeGen, 52;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsCodeGenComplete', 'bit', @Tb_IsCodeGenComplete, 53;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsTagForCodeGen', 'bit', @Tb_IsTagForCodeGen, 54;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsTagForOther', 'bit', @Tb_IsTagForOther, 55;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsActiveRow', 'bit', @Tb_IsActiveRow, 56;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UserId', 'uniqueidentifier', @Tb_UserId, 57;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 58;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 59;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTable_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTable_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTable_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTable_put', 'spWcTable_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTable_put', 'spWcTable_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTable_put', 'spWcTable_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTable_put', 'spWcTable_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTable_put', 'spWcTable_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTable_put]
-- Script for this SP was created on: 1/7/2018 10:07:08 PM
(
@Tb_Id uniqueidentifier = NULL OUTPUT,
@Tb_ApVrsn_Id uniqueidentifier,
@Tb_Name varchar(100),
@Tb_EntityRootName varchar(75),
@Tb_VariableName varchar(100),
@Tb_ScreenCaption varchar(100),
@Tb_Description varchar(2000),
@Tb_DevelopmentNote varchar(2000),
@Tb_UseLegacyConnectionCode bit,
@Tb_DbCnSK_Id uniqueidentifier,
@Tb_ApDbScm_Id uniqueidentifier,
@Tb_UIAssemblyName varchar(75),
@Tb_UINamespace varchar(75),
@Tb_UIAssemblyPath varchar(250),
@Tb_ProxyAssemblyName varchar(75),
@Tb_ProxyNamespace varchar(100),
@Tb_ProxyAssemblyPath varchar(250),
@Tb_WireTypeAssemblyName varchar(100),
@Tb_WireTypeNamespace varchar(100),
@Tb_WireTypePath varchar(250),
@Tb_WebServiceName varchar(100),
@Tb_WebServiceFolder varchar(100),
@Tb_DataServiceAssemblyName varchar(100),
@Tb_DataServiceNamespace varchar(100),
@Tb_DataServicePath varchar(250),
@Tb_UseTilesInPropsScreen bit,
@Tb_UseGridColumnGroups bit,
@Tb_UseGridDataSourceCombo bit,
@Tb_PkIsIdentity bit,
@Tb_IsVirtual bit,
@Tb_IsScreenPlaceHolder bit,
@Tb_IsNotEntity bit,
@Tb_IsMany2Many bit,
@Tb_UseLastModifiedByUserNameInSproc bit,
@Tb_UseUserTimeStamp bit,
@Tb_UseForAudit bit,
@Tb_IsAllowDelete bit,
@Tb_CnfgGdMnu_MenuRow_IsVisible bit,
@Tb_CnfgGdMnu_GridTools_IsVisible bit,
@Tb_CnfgGdMnu_SplitScreen_IsVisible bit,
@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default bit,
@Tb_CnfgGdMnu_NavColumnWidth int,
@Tb_CnfgGdMnu_IsReadOnly bit,
@Tb_CnfgGdMnu_IsAllowNewRow bit,
@Tb_CnfgGdMnu_ExcelExport_IsVisible bit,
@Tb_CnfgGdMnu_ColumnChooser_IsVisible bit,
@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible bit,
@Tb_CnfgGdMnu_RefreshGrid_IsVisible bit,
@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible bit,
@Tb_IsInputComplete bit,
@Tb_IsCodeGen bit,
@Tb_IsReadyCodeGen bit,
@Tb_IsCodeGenComplete bit,
@Tb_IsTagForCodeGen bit,
@Tb_IsTagForOther bit,
@Tb_IsActiveRow bit,
@Tb_UserId uniqueidentifier,
@Tb_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@Tb_Id Is NULL)
	BEGIN
		SET @Tb_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcTable WHERE Tb_Id = @Tb_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcTable_putInsert'
		EXEC spWcTable_putInsert
		@Tb_Id,
		@Tb_ApVrsn_Id,
		@Tb_Name,
		@Tb_EntityRootName,
		@Tb_VariableName,
		@Tb_ScreenCaption,
		@Tb_Description,
		@Tb_DevelopmentNote,
		@Tb_UseLegacyConnectionCode,
		@Tb_DbCnSK_Id,
		@Tb_ApDbScm_Id,
		@Tb_UIAssemblyName,
		@Tb_UINamespace,
		@Tb_UIAssemblyPath,
		@Tb_ProxyAssemblyName,
		@Tb_ProxyNamespace,
		@Tb_ProxyAssemblyPath,
		@Tb_WireTypeAssemblyName,
		@Tb_WireTypeNamespace,
		@Tb_WireTypePath,
		@Tb_WebServiceName,
		@Tb_WebServiceFolder,
		@Tb_DataServiceAssemblyName,
		@Tb_DataServiceNamespace,
		@Tb_DataServicePath,
		@Tb_UseTilesInPropsScreen,
		@Tb_UseGridColumnGroups,
		@Tb_UseGridDataSourceCombo,
		@Tb_PkIsIdentity,
		@Tb_IsVirtual,
		@Tb_IsScreenPlaceHolder,
		@Tb_IsNotEntity,
		@Tb_IsMany2Many,
		@Tb_UseLastModifiedByUserNameInSproc,
		@Tb_UseUserTimeStamp,
		@Tb_UseForAudit,
		@Tb_IsAllowDelete,
		@Tb_CnfgGdMnu_MenuRow_IsVisible,
		@Tb_CnfgGdMnu_GridTools_IsVisible,
		@Tb_CnfgGdMnu_SplitScreen_IsVisible,
		@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default,
		@Tb_CnfgGdMnu_NavColumnWidth,
		@Tb_CnfgGdMnu_IsReadOnly,
		@Tb_CnfgGdMnu_IsAllowNewRow,
		@Tb_CnfgGdMnu_ExcelExport_IsVisible,
		@Tb_CnfgGdMnu_ColumnChooser_IsVisible,
		@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible,
		@Tb_CnfgGdMnu_RefreshGrid_IsVisible,
		@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible,
		@Tb_IsInputComplete,
		@Tb_IsCodeGen,
		@Tb_IsReadyCodeGen,
		@Tb_IsCodeGenComplete,
		@Tb_IsTagForCodeGen,
		@Tb_IsTagForOther,
		@Tb_IsActiveRow,
		@Tb_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @Tb_Stamp =( SELECT Tb_Stamp FROM wcTable WHERE (Tb_Id = @Tb_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcTable WHERE (Tb_Id = @Tb_Id) AND (Tb_Stamp = @Tb_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcTable_putUpdate'
				EXEC spWcTable_putUpdate
				@Tb_Id,
				@Tb_ApVrsn_Id,
				@Tb_Name,
				@Tb_EntityRootName,
				@Tb_VariableName,
				@Tb_ScreenCaption,
				@Tb_Description,
				@Tb_DevelopmentNote,
				@Tb_UseLegacyConnectionCode,
				@Tb_DbCnSK_Id,
				@Tb_ApDbScm_Id,
				@Tb_UIAssemblyName,
				@Tb_UINamespace,
				@Tb_UIAssemblyPath,
				@Tb_ProxyAssemblyName,
				@Tb_ProxyNamespace,
				@Tb_ProxyAssemblyPath,
				@Tb_WireTypeAssemblyName,
				@Tb_WireTypeNamespace,
				@Tb_WireTypePath,
				@Tb_WebServiceName,
				@Tb_WebServiceFolder,
				@Tb_DataServiceAssemblyName,
				@Tb_DataServiceNamespace,
				@Tb_DataServicePath,
				@Tb_UseTilesInPropsScreen,
				@Tb_UseGridColumnGroups,
				@Tb_UseGridDataSourceCombo,
				@Tb_PkIsIdentity,
				@Tb_IsVirtual,
				@Tb_IsScreenPlaceHolder,
				@Tb_IsNotEntity,
				@Tb_IsMany2Many,
				@Tb_UseLastModifiedByUserNameInSproc,
				@Tb_UseUserTimeStamp,
				@Tb_UseForAudit,
				@Tb_IsAllowDelete,
				@Tb_CnfgGdMnu_MenuRow_IsVisible,
				@Tb_CnfgGdMnu_GridTools_IsVisible,
				@Tb_CnfgGdMnu_SplitScreen_IsVisible,
				@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default,
				@Tb_CnfgGdMnu_NavColumnWidth,
				@Tb_CnfgGdMnu_IsReadOnly,
				@Tb_CnfgGdMnu_IsAllowNewRow,
				@Tb_CnfgGdMnu_ExcelExport_IsVisible,
				@Tb_CnfgGdMnu_ColumnChooser_IsVisible,
				@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible,
				@Tb_CnfgGdMnu_RefreshGrid_IsVisible,
				@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible,
				@Tb_IsInputComplete,
				@Tb_IsCodeGen,
				@Tb_IsReadyCodeGen,
				@Tb_IsCodeGenComplete,
				@Tb_IsTagForCodeGen,
				@Tb_IsTagForOther,
				@Tb_IsActiveRow,
				@Tb_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @Tb_Stamp =( SELECT Tb_Stamp FROM wcTable WHERE (Tb_Id = @Tb_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_Id', 'uniqueidentifier', @Tb_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ApVrsn_Id', 'uniqueidentifier', @Tb_ApVrsn_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_Name', 'varchar', @Tb_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_EntityRootName', 'varchar', @Tb_EntityRootName, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_VariableName', 'varchar', @Tb_VariableName, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ScreenCaption', 'varchar', @Tb_ScreenCaption, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_Description', 'varchar', @Tb_Description, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DevelopmentNote', 'varchar', @Tb_DevelopmentNote, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseLegacyConnectionCode', 'bit', @Tb_UseLegacyConnectionCode, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DbCnSK_Id', 'uniqueidentifier', @Tb_DbCnSK_Id, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ApDbScm_Id', 'uniqueidentifier', @Tb_ApDbScm_Id, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UIAssemblyName', 'varchar', @Tb_UIAssemblyName, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UINamespace', 'varchar', @Tb_UINamespace, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UIAssemblyPath', 'varchar', @Tb_UIAssemblyPath, 14;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ProxyAssemblyName', 'varchar', @Tb_ProxyAssemblyName, 15;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ProxyNamespace', 'varchar', @Tb_ProxyNamespace, 16;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_ProxyAssemblyPath', 'varchar', @Tb_ProxyAssemblyPath, 17;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WireTypeAssemblyName', 'varchar', @Tb_WireTypeAssemblyName, 18;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WireTypeNamespace', 'varchar', @Tb_WireTypeNamespace, 19;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WireTypePath', 'varchar', @Tb_WireTypePath, 20;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WebServiceName', 'varchar', @Tb_WebServiceName, 21;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_WebServiceFolder', 'varchar', @Tb_WebServiceFolder, 22;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DataServiceAssemblyName', 'varchar', @Tb_DataServiceAssemblyName, 23;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DataServiceNamespace', 'varchar', @Tb_DataServiceNamespace, 24;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_DataServicePath', 'varchar', @Tb_DataServicePath, 25;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseTilesInPropsScreen', 'bit', @Tb_UseTilesInPropsScreen, 26;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseGridColumnGroups', 'bit', @Tb_UseGridColumnGroups, 27;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseGridDataSourceCombo', 'bit', @Tb_UseGridDataSourceCombo, 28;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_PkIsIdentity', 'bit', @Tb_PkIsIdentity, 29;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsVirtual', 'bit', @Tb_IsVirtual, 30;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsScreenPlaceHolder', 'bit', @Tb_IsScreenPlaceHolder, 31;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsNotEntity', 'bit', @Tb_IsNotEntity, 32;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsMany2Many', 'bit', @Tb_IsMany2Many, 33;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseLastModifiedByUserNameInSproc', 'bit', @Tb_UseLastModifiedByUserNameInSproc, 34;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseUserTimeStamp', 'bit', @Tb_UseUserTimeStamp, 35;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UseForAudit', 'bit', @Tb_UseForAudit, 36;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsAllowDelete', 'bit', @Tb_IsAllowDelete, 37;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_MenuRow_IsVisible', 'bit', @Tb_CnfgGdMnu_MenuRow_IsVisible, 38;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_GridTools_IsVisible', 'bit', @Tb_CnfgGdMnu_GridTools_IsVisible, 39;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_SplitScreen_IsVisible', 'bit', @Tb_CnfgGdMnu_SplitScreen_IsVisible, 40;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_SplitScreen_IsSplit_Default', 'bit', @Tb_CnfgGdMnu_SplitScreen_IsSplit_Default, 41;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_NavColumnWidth', 'int', @Tb_CnfgGdMnu_NavColumnWidth, 42;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_IsReadOnly', 'bit', @Tb_CnfgGdMnu_IsReadOnly, 43;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_IsAllowNewRow', 'bit', @Tb_CnfgGdMnu_IsAllowNewRow, 44;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_ExcelExport_IsVisible', 'bit', @Tb_CnfgGdMnu_ExcelExport_IsVisible, 45;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_ColumnChooser_IsVisible', 'bit', @Tb_CnfgGdMnu_ColumnChooser_IsVisible, 46;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_ShowHideColBtn_IsVisible', 'bit', @Tb_CnfgGdMnu_ShowHideColBtn_IsVisible, 47;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_RefreshGrid_IsVisible', 'bit', @Tb_CnfgGdMnu_RefreshGrid_IsVisible, 48;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible', 'bit', @Tb_CnfgGdMnu_CollapsAllParentNavGrids_IsVisible, 49;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsInputComplete', 'bit', @Tb_IsInputComplete, 50;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsCodeGen', 'bit', @Tb_IsCodeGen, 51;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsReadyCodeGen', 'bit', @Tb_IsReadyCodeGen, 52;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsCodeGenComplete', 'bit', @Tb_IsCodeGenComplete, 53;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsTagForCodeGen', 'bit', @Tb_IsTagForCodeGen, 54;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsTagForOther', 'bit', @Tb_IsTagForOther, 55;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_IsActiveRow', 'bit', @Tb_IsActiveRow, 56;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Tb_UserId', 'uniqueidentifier', @Tb_UserId, 57;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 58;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 59;

END CATCH
	
RETURN
GO


--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcTable')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcTable', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


