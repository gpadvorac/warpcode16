-- wcStoredProcParamValueGroup


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcStoredProcParamValueGroup

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValueGroup_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValueGroup_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_lst', 'spWcStoredProcParamValueGroup_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_lst', 'spWcStoredProcParamValueGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValueGroup_lst', 'spWcStoredProcParamValueGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValueGroup_lst', 'spWcStoredProcParamValueGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValueGroup_lst', 'spWcStoredProcParamValueGroup_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValueGroup_lst]
-- Script for this SP was created on: 12/26/2017 8:02:12 PM
(
@SpPVGrp_Sp_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
SpPVGrp_Id,
SpPVGrp_Sp_Id,
SpPVGrp_Name,
SpPVGrp_Notes,
SpPVGrp_IsActiveRow,
SpPVGrp_IsDeleted,
SpPVGrp_CreatedUserId,
SpPVGrp_CreatedDate,
SpPVGrp_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

SpPVGrp_LastModifiedDate,
SpPVGrp_Stamp


FROM 		wcStoredProcParamValueGroup LEFT OUTER JOIN
                tbPerson ON wcStoredProcParamValueGroup.SpPVGrp_UserId = tbPerson.Pn_SecurityUserId


WHERE   (SpPVGrp_Sp_Id = @SpPVGrp_Sp_Id) AND (SpPVGrp_IsDeleted = 0)

ORDER BY    SpPVGrp_Name ASC


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValueGroup_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValueGroup_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_row', 'spWcStoredProcParamValueGroup_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_row', 'spWcStoredProcParamValueGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValueGroup_row', 'spWcStoredProcParamValueGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValueGroup_row', 'spWcStoredProcParamValueGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValueGroup_row', 'spWcStoredProcParamValueGroup_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValueGroup_row]
-- Script for this SP was created on: 12/26/2017 8:02:12 PM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
SpPVGrp_Id,
SpPVGrp_Sp_Id,
SpPVGrp_Name,
SpPVGrp_Notes,
SpPVGrp_IsActiveRow,
SpPVGrp_IsDeleted,
SpPVGrp_CreatedUserId,
SpPVGrp_CreatedDate,
SpPVGrp_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

SpPVGrp_LastModifiedDate,
SpPVGrp_Stamp


FROM 		wcStoredProcParamValueGroup LEFT OUTER JOIN
                tbPerson ON wcStoredProcParamValueGroup.SpPVGrp_UserId = tbPerson.Pn_SecurityUserId


WHERE   (SpPVGrp_Id = @Id)

ORDER BY    SpPVGrp_Name ASC


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValueGroup_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValueGroup_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_lstAll', 'spWcStoredProcParamValueGroup_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_lstAll', 'spWcStoredProcParamValueGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValueGroup_lstAll', 'spWcStoredProcParamValueGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValueGroup_lstAll', 'spWcStoredProcParamValueGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValueGroup_lstAll', 'spWcStoredProcParamValueGroup_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValueGroup_lstAll]
-- Script for this SP was created on: 12/26/2017 8:02:12 PM
AS
SET NOCOUNT ON

SELECT 	
SpPVGrp_Id,
SpPVGrp_Sp_Id,
SpPVGrp_Name,
SpPVGrp_Notes,
SpPVGrp_IsActiveRow,
SpPVGrp_IsDeleted,
SpPVGrp_CreatedUserId,
SpPVGrp_CreatedDate,
SpPVGrp_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

SpPVGrp_LastModifiedDate,
SpPVGrp_Stamp


FROM 		wcStoredProcParamValueGroup LEFT OUTER JOIN
                tbPerson ON wcStoredProcParamValueGroup.SpPVGrp_UserId = tbPerson.Pn_SecurityUserId

WHERE   (SpPVGrp_IsDeleted = 0)

ORDER BY    SpPVGrp_Name ASC


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValueGroup_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValueGroup_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_putInsert', 'spWcStoredProcParamValueGroup_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_putInsert', 'spWcStoredProcParamValueGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValueGroup_putInsert', 'spWcStoredProcParamValueGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValueGroup_putInsert', 'spWcStoredProcParamValueGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValueGroup_putInsert', 'spWcStoredProcParamValueGroup_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValueGroup_putInsert]
-- Script for this SP was created on: 12/26/2017 8:02:12 PM
(
@SpPVGrp_Id uniqueidentifier = NULL OUTPUT,
@SpPVGrp_Sp_Id uniqueidentifier,
@SpPVGrp_Name varchar(50),
@SpPVGrp_Notes varchar(255),
@SpPVGrp_IsActiveRow bit,
@SpPVGrp_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcStoredProcParamValueGroup'
      INSERT INTO wcStoredProcParamValueGroup
          (
		SpPVGrp_Id,
		SpPVGrp_Sp_Id,
		SpPVGrp_Name,
		SpPVGrp_Notes,
		SpPVGrp_IsActiveRow,
		SpPVGrp_CreatedUserId,
		SpPVGrp_CreatedDate,
		SpPVGrp_UserId,
		SpPVGrp_LastModifiedDate
				)
VALUES	(
		@SpPVGrp_Id, 
		@SpPVGrp_Sp_Id, 
		RTRIM(LTRIM(@SpPVGrp_Name)), 
		RTRIM(LTRIM(@SpPVGrp_Notes)), 
		@SpPVGrp_IsActiveRow, 
		@SpPVGrp_UserId, 
		GETDATE(), 
		@SpPVGrp_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Id', 'uniqueidentifier', @SpPVGrp_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Sp_Id', 'uniqueidentifier', @SpPVGrp_Sp_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Name', 'varchar', @SpPVGrp_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Notes', 'varchar', @SpPVGrp_Notes, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_IsActiveRow', 'bit', @SpPVGrp_IsActiveRow, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_UserId', 'uniqueidentifier', @SpPVGrp_UserId, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 8;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValueGroup_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValueGroup_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_putUpdate', 'spWcStoredProcParamValueGroup_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_putUpdate', 'spWcStoredProcParamValueGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValueGroup_putUpdate', 'spWcStoredProcParamValueGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValueGroup_putUpdate', 'spWcStoredProcParamValueGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValueGroup_putUpdate', 'spWcStoredProcParamValueGroup_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValueGroup_putUpdate]
-- Script for this SP was created on: 12/26/2017 8:02:12 PM
(
@SpPVGrp_Id uniqueidentifier = NULL OUTPUT,
@SpPVGrp_Sp_Id uniqueidentifier,
@SpPVGrp_Name varchar(50),
@SpPVGrp_Notes varchar(255),
@SpPVGrp_IsActiveRow bit,
@SpPVGrp_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcStoredProcParamValueGroup'
		UPDATE	wcStoredProcParamValueGroup
		SET
		SpPVGrp_Name = RTRIM(LTRIM(@SpPVGrp_Name)),
		SpPVGrp_Notes = RTRIM(LTRIM(@SpPVGrp_Notes)),
		SpPVGrp_IsActiveRow = @SpPVGrp_IsActiveRow,
		SpPVGrp_UserId = @SpPVGrp_UserId,
		SpPVGrp_LastModifiedDate = GETDATE()
		WHERE	(SpPVGrp_Id=@SpPVGrp_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Id', 'uniqueidentifier', @SpPVGrp_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Sp_Id', 'uniqueidentifier', @SpPVGrp_Sp_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Name', 'varchar', @SpPVGrp_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Notes', 'varchar', @SpPVGrp_Notes, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_IsActiveRow', 'bit', @SpPVGrp_IsActiveRow, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_UserId', 'uniqueidentifier', @SpPVGrp_UserId, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 8;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValueGroup_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValueGroup_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_put', 'spWcStoredProcParamValueGroup_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_put', 'spWcStoredProcParamValueGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValueGroup_put', 'spWcStoredProcParamValueGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValueGroup_put', 'spWcStoredProcParamValueGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValueGroup_put', 'spWcStoredProcParamValueGroup_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValueGroup_put]
-- Script for this SP was created on: 12/26/2017 8:02:12 PM
(
@SpPVGrp_Id uniqueidentifier = NULL OUTPUT,
@SpPVGrp_Sp_Id uniqueidentifier,
@SpPVGrp_Name varchar(50),
@SpPVGrp_Notes varchar(255),
@SpPVGrp_IsActiveRow bit,
@SpPVGrp_UserId uniqueidentifier,
@SpPVGrp_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@SpPVGrp_Id Is NULL)
	BEGIN
		SET @SpPVGrp_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcStoredProcParamValueGroup WHERE SpPVGrp_Id = @SpPVGrp_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcStoredProcParamValueGroup_putInsert'
		EXEC spWcStoredProcParamValueGroup_putInsert
		@SpPVGrp_Id,
		@SpPVGrp_Sp_Id,
		@SpPVGrp_Name,
		@SpPVGrp_Notes,
		@SpPVGrp_IsActiveRow,
		@SpPVGrp_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @SpPVGrp_Stamp =( SELECT SpPVGrp_Stamp FROM wcStoredProcParamValueGroup WHERE (SpPVGrp_Id = @SpPVGrp_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcStoredProcParamValueGroup WHERE (SpPVGrp_Id = @SpPVGrp_Id) AND (SpPVGrp_Stamp = @SpPVGrp_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcStoredProcParamValueGroup_putUpdate'
				EXEC spWcStoredProcParamValueGroup_putUpdate
				@SpPVGrp_Id,
				@SpPVGrp_Sp_Id,
				@SpPVGrp_Name,
				@SpPVGrp_Notes,
				@SpPVGrp_IsActiveRow,
				@SpPVGrp_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @SpPVGrp_Stamp =( SELECT SpPVGrp_Stamp FROM wcStoredProcParamValueGroup WHERE (SpPVGrp_Id = @SpPVGrp_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Id', 'uniqueidentifier', @SpPVGrp_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Sp_Id', 'uniqueidentifier', @SpPVGrp_Sp_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Name', 'varchar', @SpPVGrp_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_Notes', 'varchar', @SpPVGrp_Notes, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_IsActiveRow', 'bit', @SpPVGrp_IsActiveRow, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@SpPVGrp_UserId', 'uniqueidentifier', @SpPVGrp_UserId, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 8;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProcParamValueGroup_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProcParamValueGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProcParamValueGroup_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_putIsDeleted', 'spWcStoredProcParamValueGroup_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProcParamValueGroup_putIsDeleted', 'spWcStoredProcParamValueGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProcParamValueGroup_putIsDeleted', 'spWcStoredProcParamValueGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProcParamValueGroup_putIsDeleted', 'spWcStoredProcParamValueGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProcParamValueGroup_putIsDeleted', 'spWcStoredProcParamValueGroup_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProcParamValueGroup_putIsDeleted]
-- Script for this SP was created on: 12/26/2017 8:02:12 PM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcStoredProcParamValueGroup
	   SET          SpPVGrp_IsDeleted = @IsDeleted
	   WHERE        (SpPVGrp_Id = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN





--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcStoredProcParamValueGroup')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcStoredProcParamValueGroup', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


