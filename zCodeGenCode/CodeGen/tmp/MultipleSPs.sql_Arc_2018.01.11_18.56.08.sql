-- wcApplicationVersion


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcApplicationVersion

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcApplicationVersion_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcApplicationVersion_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcApplicationVersion_row', 'spWcApplicationVersion_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcApplicationVersion_row', 'spWcApplicationVersion_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcApplicationVersion_row', 'spWcApplicationVersion_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcApplicationVersion_row', 'spWcApplicationVersion_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcApplicationVersion_row', 'spWcApplicationVersion_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcApplicationVersion_row]
-- Script for this SP was created on: 1/11/2018 6:53:41 PM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
ApVrsn_Id,
ApVrsn_Ap_Id,
AttachmentCount,
AttachmentFileNames,
DiscussionCount,
DiscussionTitles,
ApVrsn_MajorVersion,
ApVrsn_MinorVersion,
ApVrsn_VersionIteration,
ApVrsn_DbName,
ApVrsn_Server,
ApVrsn_UseLegacyConnectionCode,
ApVrsn_SolutionPath,
ApVrsn_DefaultUIAssembly,
ApVrsn_DefaultUINamespace,
ApVrsn_DefaultUIAssemblyPath,
ApVrsn_ProxyAssemblyName,
ApVrsn_ProxyNamespace,
ApVrsn_ProxyAssemblyPath,
ApVrsn_DefaultWireTypeAssembly,
ApVrsn_DefaultWireTypeNamespace,
ApVrsn_DefaultWireTypePath,
ApVrsn_WebServerURL,
ApVrsn_WebsiteCodeFolderPath,
ApVrsn_WebserviceCodeFolderPath,
ApVrsn_StoredProcCodeFolder,
ApVrsn_DataServiceAssemblyName,
ApVrsn_DataServiceNamespace,
ApVrsn_DataServicePath,
ApVrsn_IsMulticultural,
ApVrsn_Notes,
ApVrsn_IsActiveRow,
ApVrsn_IsDeleted,
ApVrsn_CreatedUserId,
ApVrsn_CreatedDate,
ApVrsn_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

ApVrsn_LastModifiedDate,
ApVrsn_Stamp


FROM 		wcApplicationVersion LEFT OUTER JOIN
                tbPerson ON wcApplicationVersion.ApVrsn_UserId = tbPerson.Pn_SecurityUserId


WHERE   (ApVrsn_Id = @Id)

ORDER BY    ApVrsn_MajorVersion, ApVrsn_MinorVersion, ApVrsn_VersionIteration


RETURN

GO

--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcApplicationVersion')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcApplicationVersion', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


