

/*

SELECT name
FROM OPENROWSET
(
  'SQLNCLI', 
  'Server=Voyager3;Trusted_Connection=yes;',
 'select WarpCodeV16'
); 

*/


-- wcStoredProc


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcStoredProc

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProc_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProc_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProc_row', 'spWcStoredProc_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProc_row', 'spWcStoredProc_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProc_row', 'spWcStoredProc_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProc_row', 'spWcStoredProc_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProc_row', 'spWcStoredProc_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProc_row]
-- Script for this SP was created on: 12/23/2017 1:27:29 AM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
Sp_Id,
Sp_ApVrsn_Id,
Sp_Name,
Sp_IsCodeGen,
Sp_IsReadyCodeGen,
Sp_IsCodeGenComplete,
Sp_IsTagForCodeGen,
Sp_IsTagForOther,
Sp_SprocGroups,
Sp_IsBuildDataAccessMethod,
Sp_BuildListClass,
Sp_IsFetchEntity,
Sp_IsStaticList,
Sp_IsTypeComboItem,
Sp_IsCreateWireType,
Sp_WireTypeName,
Sp_MethodName,
Sp_AssocEntity_Id,
Tb_Name AS Sp_AssocEntity_Id_TextField,

Sp_AssocEntityNames,
Sp_SpRsTp_Id,
SpRsTp_Name AS Sp_SpRsTp_Id_TextField,

Sp_SpRtTp_Id,
SpRtTp_Name AS Sp_SpRtTp_Id_TextField,

Sp_IsInputParamsAsObjectArray,
Sp_IsParamsAutoRefresh,
Sp_HasNewParams,
Sp_IsParamsValid,
Sp_IsParamValueSetValid,
Sp_HasNewColumns,
Sp_IsColumnsValid,
Sp_IsValid,
Sp_Notes,
Sp_IsActiveRow,
Sp_IsDeleted,
Sp_CreatedUserId,
Sp_CreatedDate,
Sp_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

Sp_LastModifiedDate,
Sp_Stamp


FROM 		wcStoredProc LEFT OUTER JOIN
                tbPerson ON wcStoredProc.Sp_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTable ON wcStoredProc.Sp_AssocEntity_Id = wcTable.Tb_Id LEFT OUTER JOIN
                wcStoredProcResultType ON wcStoredProc.Sp_SpRsTp_Id = wcStoredProcResultType.SpRsTp_Id LEFT OUTER JOIN
                wcStoredProcReturnType ON wcStoredProc.Sp_SpRtTp_Id = wcStoredProcReturnType.SpRtTp_Id


WHERE   (Sp_Id = @Id)

ORDER BY    Sp_Name ASC


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProc_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProc_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProc_lst', 'spWcStoredProc_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProc_lst', 'spWcStoredProc_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProc_lst', 'spWcStoredProc_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProc_lst', 'spWcStoredProc_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProc_lst', 'spWcStoredProc_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProc_lst]
-- Script for this SP was created on: 12/23/2017 1:27:29 AM
(
@Sp_ApVrsn_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
Sp_Id,
Sp_ApVrsn_Id,
Sp_Name,
Sp_IsCodeGen,
Sp_IsReadyCodeGen,
Sp_IsCodeGenComplete,
Sp_IsTagForCodeGen,
Sp_IsTagForOther,
Sp_SprocGroups,
Sp_IsBuildDataAccessMethod,
Sp_BuildListClass,
Sp_IsFetchEntity,
Sp_IsStaticList,
Sp_IsTypeComboItem,
Sp_IsCreateWireType,
Sp_WireTypeName,
Sp_MethodName,
Sp_AssocEntity_Id,
Tb_Name AS Sp_AssocEntity_Id_TextField,

Sp_AssocEntityNames,
Sp_SpRsTp_Id,
SpRsTp_Name AS Sp_SpRsTp_Id_TextField,

Sp_SpRtTp_Id,
SpRtTp_Name AS Sp_SpRtTp_Id_TextField,

Sp_IsInputParamsAsObjectArray,
Sp_IsParamsAutoRefresh,
Sp_HasNewParams,
Sp_IsParamsValid,
Sp_IsParamValueSetValid,
Sp_HasNewColumns,
Sp_IsColumnsValid,
Sp_IsValid,
Sp_Notes,
Sp_IsActiveRow,
Sp_IsDeleted,
Sp_CreatedUserId,
Sp_CreatedDate,
Sp_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

Sp_LastModifiedDate,
Sp_Stamp


FROM 		wcStoredProc LEFT OUTER JOIN
                tbPerson ON wcStoredProc.Sp_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTable ON wcStoredProc.Sp_AssocEntity_Id = wcTable.Tb_Id LEFT OUTER JOIN
                wcStoredProcResultType ON wcStoredProc.Sp_SpRsTp_Id = wcStoredProcResultType.SpRsTp_Id LEFT OUTER JOIN
                wcStoredProcReturnType ON wcStoredProc.Sp_SpRtTp_Id = wcStoredProcReturnType.SpRtTp_Id


WHERE   (Sp_ApVrsn_Id = @Sp_ApVrsn_Id) AND (Sp_IsDeleted = 0)

ORDER BY    Sp_Name ASC


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProc_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProc_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProc_lstAll', 'spWcStoredProc_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProc_lstAll', 'spWcStoredProc_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProc_lstAll', 'spWcStoredProc_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProc_lstAll', 'spWcStoredProc_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProc_lstAll', 'spWcStoredProc_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProc_lstAll]
-- Script for this SP was created on: 12/23/2017 1:27:29 AM
AS
SET NOCOUNT ON

SELECT 	
Sp_Id,
Sp_ApVrsn_Id,
Sp_Name,
Sp_IsCodeGen,
Sp_IsReadyCodeGen,
Sp_IsCodeGenComplete,
Sp_IsTagForCodeGen,
Sp_IsTagForOther,
Sp_SprocGroups,
Sp_IsBuildDataAccessMethod,
Sp_BuildListClass,
Sp_IsFetchEntity,
Sp_IsStaticList,
Sp_IsTypeComboItem,
Sp_IsCreateWireType,
Sp_WireTypeName,
Sp_MethodName,
Sp_AssocEntity_Id,
Tb_Name AS Sp_AssocEntity_Id_TextField,

Sp_AssocEntityNames,
Sp_SpRsTp_Id,
SpRsTp_Name AS Sp_SpRsTp_Id_TextField,

Sp_SpRtTp_Id,
SpRtTp_Name AS Sp_SpRtTp_Id_TextField,

Sp_IsInputParamsAsObjectArray,
Sp_IsParamsAutoRefresh,
Sp_HasNewParams,
Sp_IsParamsValid,
Sp_IsParamValueSetValid,
Sp_HasNewColumns,
Sp_IsColumnsValid,
Sp_IsValid,
Sp_Notes,
Sp_IsActiveRow,
Sp_IsDeleted,
Sp_CreatedUserId,
Sp_CreatedDate,
Sp_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

Sp_LastModifiedDate,
Sp_Stamp


FROM 		wcStoredProc LEFT OUTER JOIN
                tbPerson ON wcStoredProc.Sp_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTable ON wcStoredProc.Sp_AssocEntity_Id = wcTable.Tb_Id LEFT OUTER JOIN
                wcStoredProcResultType ON wcStoredProc.Sp_SpRsTp_Id = wcStoredProcResultType.SpRsTp_Id LEFT OUTER JOIN
                wcStoredProcReturnType ON wcStoredProc.Sp_SpRtTp_Id = wcStoredProcReturnType.SpRtTp_Id

WHERE   (Sp_IsDeleted = 0)

ORDER BY    Sp_Name ASC


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProc_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProc_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProc_putUpdate', 'spWcStoredProc_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProc_putUpdate', 'spWcStoredProc_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProc_putUpdate', 'spWcStoredProc_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProc_putUpdate', 'spWcStoredProc_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProc_putUpdate', 'spWcStoredProc_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProc_putUpdate]
-- Script for this SP was created on: 12/23/2017 1:27:29 AM
(
@Sp_Id uniqueidentifier = NULL OUTPUT,
@Sp_ApVrsn_Id uniqueidentifier,
@Sp_Name varchar(128),
@Sp_IsCodeGen bit,
@Sp_IsReadyCodeGen bit,
@Sp_IsCodeGenComplete bit,
@Sp_IsTagForCodeGen bit,
@Sp_IsTagForOther bit,
@Sp_SprocGroups varchar(500),
@Sp_IsBuildDataAccessMethod bit,
@Sp_BuildListClass bit,
@Sp_IsFetchEntity bit,
@Sp_IsStaticList bit,
@Sp_IsTypeComboItem bit,
@Sp_IsCreateWireType bit,
@Sp_WireTypeName varchar(100),
@Sp_MethodName varchar(75),
@Sp_AssocEntity_Id uniqueidentifier,
@Sp_SpRsTp_Id int,
@Sp_SpRtTp_Id int,
@Sp_IsInputParamsAsObjectArray bit,
@Sp_IsParamsAutoRefresh bit,
@Sp_HasNewParams bit,
@Sp_IsParamsValid bit,
@Sp_IsParamValueSetValid bit,
@Sp_HasNewColumns bit,
@Sp_IsColumnsValid bit,
@Sp_IsValid bit,
@Sp_Notes varchar(255),
@Sp_IsActiveRow bit,
@Sp_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcStoredProc'
		UPDATE	wcStoredProc
		SET
		Sp_Name = RTRIM(LTRIM(@Sp_Name)),
		Sp_IsCodeGen = @Sp_IsCodeGen,
		Sp_IsReadyCodeGen = @Sp_IsReadyCodeGen,
		Sp_IsCodeGenComplete = @Sp_IsCodeGenComplete,
		Sp_IsTagForCodeGen = @Sp_IsTagForCodeGen,
		Sp_IsTagForOther = @Sp_IsTagForOther,
		Sp_SprocGroups = RTRIM(LTRIM(@Sp_SprocGroups)),
		Sp_IsBuildDataAccessMethod = @Sp_IsBuildDataAccessMethod,
		Sp_BuildListClass = @Sp_BuildListClass,
		Sp_IsFetchEntity = @Sp_IsFetchEntity,
		Sp_IsStaticList = @Sp_IsStaticList,
		Sp_IsTypeComboItem = @Sp_IsTypeComboItem,
		Sp_IsCreateWireType = @Sp_IsCreateWireType,
		Sp_WireTypeName = RTRIM(LTRIM(@Sp_WireTypeName)),
		Sp_MethodName = RTRIM(LTRIM(@Sp_MethodName)),
		Sp_AssocEntity_Id = @Sp_AssocEntity_Id,
		Sp_SpRsTp_Id = @Sp_SpRsTp_Id,
		Sp_SpRtTp_Id = @Sp_SpRtTp_Id,
		Sp_IsInputParamsAsObjectArray = @Sp_IsInputParamsAsObjectArray,
		Sp_IsParamsAutoRefresh = @Sp_IsParamsAutoRefresh,
		Sp_HasNewParams = @Sp_HasNewParams,
		Sp_IsParamsValid = @Sp_IsParamsValid,
		Sp_IsParamValueSetValid = @Sp_IsParamValueSetValid,
		Sp_HasNewColumns = @Sp_HasNewColumns,
		Sp_IsColumnsValid = @Sp_IsColumnsValid,
		Sp_IsValid = @Sp_IsValid,
		Sp_Notes = RTRIM(LTRIM(@Sp_Notes)),
		Sp_IsActiveRow = @Sp_IsActiveRow,
		Sp_UserId = @Sp_UserId,
		Sp_LastModifiedDate = GETDATE()
		WHERE	(Sp_Id=@Sp_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_Id', 'uniqueidentifier', @Sp_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_ApVrsn_Id', 'uniqueidentifier', @Sp_ApVrsn_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_Name', 'varchar', @Sp_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsCodeGen', 'bit', @Sp_IsCodeGen, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsReadyCodeGen', 'bit', @Sp_IsReadyCodeGen, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsCodeGenComplete', 'bit', @Sp_IsCodeGenComplete, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsTagForCodeGen', 'bit', @Sp_IsTagForCodeGen, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsTagForOther', 'bit', @Sp_IsTagForOther, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_SprocGroups', 'varchar', @Sp_SprocGroups, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsBuildDataAccessMethod', 'bit', @Sp_IsBuildDataAccessMethod, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_BuildListClass', 'bit', @Sp_BuildListClass, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsFetchEntity', 'bit', @Sp_IsFetchEntity, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsStaticList', 'bit', @Sp_IsStaticList, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsTypeComboItem', 'bit', @Sp_IsTypeComboItem, 14;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsCreateWireType', 'bit', @Sp_IsCreateWireType, 15;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_WireTypeName', 'varchar', @Sp_WireTypeName, 16;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_MethodName', 'varchar', @Sp_MethodName, 17;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_AssocEntity_Id', 'uniqueidentifier', @Sp_AssocEntity_Id, 18;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_SpRsTp_Id', 'int', @Sp_SpRsTp_Id, 19;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_SpRtTp_Id', 'int', @Sp_SpRtTp_Id, 20;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsInputParamsAsObjectArray', 'bit', @Sp_IsInputParamsAsObjectArray, 21;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsParamsAutoRefresh', 'bit', @Sp_IsParamsAutoRefresh, 22;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_HasNewParams', 'bit', @Sp_HasNewParams, 23;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsParamsValid', 'bit', @Sp_IsParamsValid, 24;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsParamValueSetValid', 'bit', @Sp_IsParamValueSetValid, 25;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_HasNewColumns', 'bit', @Sp_HasNewColumns, 26;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsColumnsValid', 'bit', @Sp_IsColumnsValid, 27;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsValid', 'bit', @Sp_IsValid, 28;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_Notes', 'varchar', @Sp_Notes, 29;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsActiveRow', 'bit', @Sp_IsActiveRow, 30;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_UserId', 'uniqueidentifier', @Sp_UserId, 31;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 32;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 33;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProc_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProc_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProc_putInsert', 'spWcStoredProc_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProc_putInsert', 'spWcStoredProc_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProc_putInsert', 'spWcStoredProc_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProc_putInsert', 'spWcStoredProc_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProc_putInsert', 'spWcStoredProc_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProc_putInsert]
-- Script for this SP was created on: 12/23/2017 1:27:29 AM
(
@Sp_Id uniqueidentifier = NULL OUTPUT,
@Sp_ApVrsn_Id uniqueidentifier,
@Sp_Name varchar(128),
@Sp_IsCodeGen bit,
@Sp_IsReadyCodeGen bit,
@Sp_IsCodeGenComplete bit,
@Sp_IsTagForCodeGen bit,
@Sp_IsTagForOther bit,
@Sp_SprocGroups varchar(500),
@Sp_IsBuildDataAccessMethod bit,
@Sp_BuildListClass bit,
@Sp_IsFetchEntity bit,
@Sp_IsStaticList bit,
@Sp_IsTypeComboItem bit,
@Sp_IsCreateWireType bit,
@Sp_WireTypeName varchar(100),
@Sp_MethodName varchar(75),
@Sp_AssocEntity_Id uniqueidentifier,
@Sp_SpRsTp_Id int,
@Sp_SpRtTp_Id int,
@Sp_IsInputParamsAsObjectArray bit,
@Sp_IsParamsAutoRefresh bit,
@Sp_HasNewParams bit,
@Sp_IsParamsValid bit,
@Sp_IsParamValueSetValid bit,
@Sp_HasNewColumns bit,
@Sp_IsColumnsValid bit,
@Sp_IsValid bit,
@Sp_Notes varchar(255),
@Sp_IsActiveRow bit,
@Sp_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcStoredProc'
      INSERT INTO wcStoredProc
          (
		Sp_Id,
		Sp_ApVrsn_Id,
		Sp_Name,
		Sp_IsCodeGen,
		Sp_IsReadyCodeGen,
		Sp_IsCodeGenComplete,
		Sp_IsTagForCodeGen,
		Sp_IsTagForOther,
		Sp_SprocGroups,
		Sp_IsBuildDataAccessMethod,
		Sp_BuildListClass,
		Sp_IsFetchEntity,
		Sp_IsStaticList,
		Sp_IsTypeComboItem,
		Sp_IsCreateWireType,
		Sp_WireTypeName,
		Sp_MethodName,
		Sp_AssocEntity_Id,
		Sp_SpRsTp_Id,
		Sp_SpRtTp_Id,
		Sp_IsInputParamsAsObjectArray,
		Sp_IsParamsAutoRefresh,
		Sp_HasNewParams,
		Sp_IsParamsValid,
		Sp_IsParamValueSetValid,
		Sp_HasNewColumns,
		Sp_IsColumnsValid,
		Sp_IsValid,
		Sp_Notes,
		Sp_IsActiveRow,
		Sp_CreatedUserId,
		Sp_CreatedDate,
		Sp_UserId,
		Sp_LastModifiedDate
				)
VALUES	(
		@Sp_Id, 
		@Sp_ApVrsn_Id, 
		RTRIM(LTRIM(@Sp_Name)), 
		@Sp_IsCodeGen, 
		@Sp_IsReadyCodeGen, 
		@Sp_IsCodeGenComplete, 
		@Sp_IsTagForCodeGen, 
		@Sp_IsTagForOther, 
		RTRIM(LTRIM(@Sp_SprocGroups)), 
		@Sp_IsBuildDataAccessMethod, 
		@Sp_BuildListClass, 
		@Sp_IsFetchEntity, 
		@Sp_IsStaticList, 
		@Sp_IsTypeComboItem, 
		@Sp_IsCreateWireType, 
		RTRIM(LTRIM(@Sp_WireTypeName)), 
		RTRIM(LTRIM(@Sp_MethodName)), 
		@Sp_AssocEntity_Id, 
		@Sp_SpRsTp_Id, 
		@Sp_SpRtTp_Id, 
		@Sp_IsInputParamsAsObjectArray, 
		@Sp_IsParamsAutoRefresh, 
		@Sp_HasNewParams, 
		@Sp_IsParamsValid, 
		@Sp_IsParamValueSetValid, 
		@Sp_HasNewColumns, 
		@Sp_IsColumnsValid, 
		@Sp_IsValid, 
		RTRIM(LTRIM(@Sp_Notes)), 
		@Sp_IsActiveRow, 
		@Sp_UserId, 
		GETDATE(), 
		@Sp_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_Id', 'uniqueidentifier', @Sp_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_ApVrsn_Id', 'uniqueidentifier', @Sp_ApVrsn_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_Name', 'varchar', @Sp_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsCodeGen', 'bit', @Sp_IsCodeGen, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsReadyCodeGen', 'bit', @Sp_IsReadyCodeGen, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsCodeGenComplete', 'bit', @Sp_IsCodeGenComplete, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsTagForCodeGen', 'bit', @Sp_IsTagForCodeGen, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsTagForOther', 'bit', @Sp_IsTagForOther, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_SprocGroups', 'varchar', @Sp_SprocGroups, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsBuildDataAccessMethod', 'bit', @Sp_IsBuildDataAccessMethod, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_BuildListClass', 'bit', @Sp_BuildListClass, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsFetchEntity', 'bit', @Sp_IsFetchEntity, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsStaticList', 'bit', @Sp_IsStaticList, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsTypeComboItem', 'bit', @Sp_IsTypeComboItem, 14;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsCreateWireType', 'bit', @Sp_IsCreateWireType, 15;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_WireTypeName', 'varchar', @Sp_WireTypeName, 16;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_MethodName', 'varchar', @Sp_MethodName, 17;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_AssocEntity_Id', 'uniqueidentifier', @Sp_AssocEntity_Id, 18;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_SpRsTp_Id', 'int', @Sp_SpRsTp_Id, 19;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_SpRtTp_Id', 'int', @Sp_SpRtTp_Id, 20;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsInputParamsAsObjectArray', 'bit', @Sp_IsInputParamsAsObjectArray, 21;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsParamsAutoRefresh', 'bit', @Sp_IsParamsAutoRefresh, 22;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_HasNewParams', 'bit', @Sp_HasNewParams, 23;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsParamsValid', 'bit', @Sp_IsParamsValid, 24;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsParamValueSetValid', 'bit', @Sp_IsParamValueSetValid, 25;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_HasNewColumns', 'bit', @Sp_HasNewColumns, 26;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsColumnsValid', 'bit', @Sp_IsColumnsValid, 27;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsValid', 'bit', @Sp_IsValid, 28;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_Notes', 'varchar', @Sp_Notes, 29;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsActiveRow', 'bit', @Sp_IsActiveRow, 30;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_UserId', 'uniqueidentifier', @Sp_UserId, 31;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 32;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 33;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProc_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProc_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProc_put', 'spWcStoredProc_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProc_put', 'spWcStoredProc_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProc_put', 'spWcStoredProc_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProc_put', 'spWcStoredProc_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProc_put', 'spWcStoredProc_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProc_put]
-- Script for this SP was created on: 12/23/2017 1:27:29 AM
(
@Sp_Id uniqueidentifier = NULL OUTPUT,
@Sp_ApVrsn_Id uniqueidentifier,
@Sp_Name varchar(128),
@Sp_IsCodeGen bit,
@Sp_IsReadyCodeGen bit,
@Sp_IsCodeGenComplete bit,
@Sp_IsTagForCodeGen bit,
@Sp_IsTagForOther bit,
@Sp_SprocGroups varchar(500),
@Sp_IsBuildDataAccessMethod bit,
@Sp_BuildListClass bit,
@Sp_IsFetchEntity bit,
@Sp_IsStaticList bit,
@Sp_IsTypeComboItem bit,
@Sp_IsCreateWireType bit,
@Sp_WireTypeName varchar(100),
@Sp_MethodName varchar(75),
@Sp_AssocEntity_Id uniqueidentifier,
@Sp_SpRsTp_Id int,
@Sp_SpRtTp_Id int,
@Sp_IsInputParamsAsObjectArray bit,
@Sp_IsParamsAutoRefresh bit,
@Sp_HasNewParams bit,
@Sp_IsParamsValid bit,
@Sp_IsParamValueSetValid bit,
@Sp_HasNewColumns bit,
@Sp_IsColumnsValid bit,
@Sp_IsValid bit,
@Sp_Notes varchar(255),
@Sp_IsActiveRow bit,
@Sp_UserId uniqueidentifier,
@Sp_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@Sp_Id Is NULL)
	BEGIN
		SET @Sp_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcStoredProc WHERE Sp_Id = @Sp_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcStoredProc_putInsert'
		EXEC spWcStoredProc_putInsert
		@Sp_Id,
		@Sp_ApVrsn_Id,
		@Sp_Name,
		@Sp_IsCodeGen,
		@Sp_IsReadyCodeGen,
		@Sp_IsCodeGenComplete,
		@Sp_IsTagForCodeGen,
		@Sp_IsTagForOther,
		@Sp_SprocGroups,
		@Sp_IsBuildDataAccessMethod,
		@Sp_BuildListClass,
		@Sp_IsFetchEntity,
		@Sp_IsStaticList,
		@Sp_IsTypeComboItem,
		@Sp_IsCreateWireType,
		@Sp_WireTypeName,
		@Sp_MethodName,
		@Sp_AssocEntity_Id,
		@Sp_SpRsTp_Id,
		@Sp_SpRtTp_Id,
		@Sp_IsInputParamsAsObjectArray,
		@Sp_IsParamsAutoRefresh,
		@Sp_HasNewParams,
		@Sp_IsParamsValid,
		@Sp_IsParamValueSetValid,
		@Sp_HasNewColumns,
		@Sp_IsColumnsValid,
		@Sp_IsValid,
		@Sp_Notes,
		@Sp_IsActiveRow,
		@Sp_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @Sp_Stamp =( SELECT Sp_Stamp FROM wcStoredProc WHERE (Sp_Id = @Sp_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcStoredProc WHERE (Sp_Id = @Sp_Id) AND (Sp_Stamp = @Sp_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcStoredProc_putUpdate'
				EXEC spWcStoredProc_putUpdate
				@Sp_Id,
				@Sp_ApVrsn_Id,
				@Sp_Name,
				@Sp_IsCodeGen,
				@Sp_IsReadyCodeGen,
				@Sp_IsCodeGenComplete,
				@Sp_IsTagForCodeGen,
				@Sp_IsTagForOther,
				@Sp_SprocGroups,
				@Sp_IsBuildDataAccessMethod,
				@Sp_BuildListClass,
				@Sp_IsFetchEntity,
				@Sp_IsStaticList,
				@Sp_IsTypeComboItem,
				@Sp_IsCreateWireType,
				@Sp_WireTypeName,
				@Sp_MethodName,
				@Sp_AssocEntity_Id,
				@Sp_SpRsTp_Id,
				@Sp_SpRtTp_Id,
				@Sp_IsInputParamsAsObjectArray,
				@Sp_IsParamsAutoRefresh,
				@Sp_HasNewParams,
				@Sp_IsParamsValid,
				@Sp_IsParamValueSetValid,
				@Sp_HasNewColumns,
				@Sp_IsColumnsValid,
				@Sp_IsValid,
				@Sp_Notes,
				@Sp_IsActiveRow,
				@Sp_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @Sp_Stamp =( SELECT Sp_Stamp FROM wcStoredProc WHERE (Sp_Id = @Sp_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_Id', 'uniqueidentifier', @Sp_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_ApVrsn_Id', 'uniqueidentifier', @Sp_ApVrsn_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_Name', 'varchar', @Sp_Name, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsCodeGen', 'bit', @Sp_IsCodeGen, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsReadyCodeGen', 'bit', @Sp_IsReadyCodeGen, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsCodeGenComplete', 'bit', @Sp_IsCodeGenComplete, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsTagForCodeGen', 'bit', @Sp_IsTagForCodeGen, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsTagForOther', 'bit', @Sp_IsTagForOther, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_SprocGroups', 'varchar', @Sp_SprocGroups, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsBuildDataAccessMethod', 'bit', @Sp_IsBuildDataAccessMethod, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_BuildListClass', 'bit', @Sp_BuildListClass, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsFetchEntity', 'bit', @Sp_IsFetchEntity, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsStaticList', 'bit', @Sp_IsStaticList, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsTypeComboItem', 'bit', @Sp_IsTypeComboItem, 14;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsCreateWireType', 'bit', @Sp_IsCreateWireType, 15;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_WireTypeName', 'varchar', @Sp_WireTypeName, 16;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_MethodName', 'varchar', @Sp_MethodName, 17;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_AssocEntity_Id', 'uniqueidentifier', @Sp_AssocEntity_Id, 18;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_SpRsTp_Id', 'int', @Sp_SpRsTp_Id, 19;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_SpRtTp_Id', 'int', @Sp_SpRtTp_Id, 20;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsInputParamsAsObjectArray', 'bit', @Sp_IsInputParamsAsObjectArray, 21;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsParamsAutoRefresh', 'bit', @Sp_IsParamsAutoRefresh, 22;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_HasNewParams', 'bit', @Sp_HasNewParams, 23;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsParamsValid', 'bit', @Sp_IsParamsValid, 24;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsParamValueSetValid', 'bit', @Sp_IsParamValueSetValid, 25;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_HasNewColumns', 'bit', @Sp_HasNewColumns, 26;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsColumnsValid', 'bit', @Sp_IsColumnsValid, 27;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsValid', 'bit', @Sp_IsValid, 28;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_Notes', 'varchar', @Sp_Notes, 29;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_IsActiveRow', 'bit', @Sp_IsActiveRow, 30;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Sp_UserId', 'uniqueidentifier', @Sp_UserId, 31;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 32;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 33;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcStoredProc_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcStoredProc_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcStoredProc_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcStoredProc_putIsDeleted', 'spWcStoredProc_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcStoredProc_putIsDeleted', 'spWcStoredProc_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcStoredProc_putIsDeleted', 'spWcStoredProc_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcStoredProc_putIsDeleted', 'spWcStoredProc_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcStoredProc_putIsDeleted', 'spWcStoredProc_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcStoredProc_putIsDeleted]
-- Script for this SP was created on: 12/23/2017 1:27:29 AM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcStoredProc
	   SET          Sp_IsDeleted = @IsDeleted
	   WHERE        (Sp_Id = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN





--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcStoredProc')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcStoredProc', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


