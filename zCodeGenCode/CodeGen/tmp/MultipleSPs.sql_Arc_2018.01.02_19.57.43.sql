

/*

SELECT name
FROM OPENROWSET
(
  'SQLNCLI', 
  'Server=Voyager3;Trusted_Connection=yes;',
 'select WarpCodeV16'
); 

*/


-- wcGridColumnGroup
-- wcGridColumnGroupItem


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcGridColumnGroup

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroup_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroup_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroup_row', 'spWcGridColumnGroup_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroup_row', 'spWcGridColumnGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroup_row', 'spWcGridColumnGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroup_row', 'spWcGridColumnGroup_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroup_row', 'spWcGridColumnGroup_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroup_row]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
GrdColGrp_Id,
GrdColGrp_Tb_Id,
GrdColGrp_Sort,
GrdColGrp_Name,
GrdColGrp_HeaderText,
GrdColGrp_IsDefaultView,
GrdColGrp_IsActiveRow,
GrdColGrp_IsDeleted,
GrdColGrp_CreatedUserId,
GrdColGrp_CreatedDate,
GrdColGrp_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

GrdColGrp_LastModifiedDate,
GrdColGrp_Stamp


FROM 		wcGridColumnGroup LEFT OUTER JOIN
                tbPerson ON wcGridColumnGroup.GrdColGrp_UserId = tbPerson.Pn_SecurityUserId


WHERE   (GrdColGrp_Id = @Id)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroup_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroup_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroup_lst', 'spWcGridColumnGroup_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroup_lst', 'spWcGridColumnGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroup_lst', 'spWcGridColumnGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroup_lst', 'spWcGridColumnGroup_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroup_lst', 'spWcGridColumnGroup_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroup_lst]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@GrdColGrp_Tb_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
GrdColGrp_Id,
GrdColGrp_Tb_Id,
GrdColGrp_Sort,
GrdColGrp_Name,
GrdColGrp_HeaderText,
GrdColGrp_IsDefaultView,
GrdColGrp_IsActiveRow,
GrdColGrp_IsDeleted,
GrdColGrp_CreatedUserId,
GrdColGrp_CreatedDate,
GrdColGrp_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

GrdColGrp_LastModifiedDate,
GrdColGrp_Stamp


FROM 		wcGridColumnGroup LEFT OUTER JOIN
                tbPerson ON wcGridColumnGroup.GrdColGrp_UserId = tbPerson.Pn_SecurityUserId


WHERE   (GrdColGrp_Tb_Id = @GrdColGrp_Tb_Id) AND (GrdColGrp_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroup_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroup_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroup_lstAll', 'spWcGridColumnGroup_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroup_lstAll', 'spWcGridColumnGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroup_lstAll', 'spWcGridColumnGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroup_lstAll', 'spWcGridColumnGroup_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroup_lstAll', 'spWcGridColumnGroup_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroup_lstAll]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
AS
SET NOCOUNT ON

SELECT 	
GrdColGrp_Id,
GrdColGrp_Tb_Id,
GrdColGrp_Sort,
GrdColGrp_Name,
GrdColGrp_HeaderText,
GrdColGrp_IsDefaultView,
GrdColGrp_IsActiveRow,
GrdColGrp_IsDeleted,
GrdColGrp_CreatedUserId,
GrdColGrp_CreatedDate,
GrdColGrp_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

GrdColGrp_LastModifiedDate,
GrdColGrp_Stamp


FROM 		wcGridColumnGroup LEFT OUTER JOIN
                tbPerson ON wcGridColumnGroup.GrdColGrp_UserId = tbPerson.Pn_SecurityUserId

WHERE   (GrdColGrp_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroup_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroup_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroup_putUpdate', 'spWcGridColumnGroup_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroup_putUpdate', 'spWcGridColumnGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroup_putUpdate', 'spWcGridColumnGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroup_putUpdate', 'spWcGridColumnGroup_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroup_putUpdate', 'spWcGridColumnGroup_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroup_putUpdate]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@GrdColGrp_Id uniqueidentifier = NULL OUTPUT,
@GrdColGrp_Tb_Id uniqueidentifier,
@GrdColGrp_Sort int,
@GrdColGrp_Name varchar(50),
@GrdColGrp_HeaderText varchar(50),
@GrdColGrp_IsDefaultView bit,
@GrdColGrp_IsActiveRow bit,
@GrdColGrp_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcGridColumnGroup'
		UPDATE	wcGridColumnGroup
		SET
		GrdColGrp_Sort = @GrdColGrp_Sort,
		GrdColGrp_Name = RTRIM(LTRIM(@GrdColGrp_Name)),
		GrdColGrp_HeaderText = RTRIM(LTRIM(@GrdColGrp_HeaderText)),
		GrdColGrp_IsDefaultView = @GrdColGrp_IsDefaultView,
		GrdColGrp_IsActiveRow = @GrdColGrp_IsActiveRow,
		GrdColGrp_UserId = @GrdColGrp_UserId,
		GrdColGrp_LastModifiedDate = GETDATE()
		WHERE	(GrdColGrp_Id=@GrdColGrp_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Id', 'uniqueidentifier', @GrdColGrp_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Tb_Id', 'uniqueidentifier', @GrdColGrp_Tb_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Sort', 'int', @GrdColGrp_Sort, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Name', 'varchar', @GrdColGrp_Name, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_HeaderText', 'varchar', @GrdColGrp_HeaderText, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_IsDefaultView', 'bit', @GrdColGrp_IsDefaultView, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_IsActiveRow', 'bit', @GrdColGrp_IsActiveRow, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_UserId', 'uniqueidentifier', @GrdColGrp_UserId, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 10;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroup_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroup_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroup_putInsert', 'spWcGridColumnGroup_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroup_putInsert', 'spWcGridColumnGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroup_putInsert', 'spWcGridColumnGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroup_putInsert', 'spWcGridColumnGroup_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroup_putInsert', 'spWcGridColumnGroup_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroup_putInsert]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@GrdColGrp_Id uniqueidentifier = NULL OUTPUT,
@GrdColGrp_Tb_Id uniqueidentifier,
@GrdColGrp_Sort int,
@GrdColGrp_Name varchar(50),
@GrdColGrp_HeaderText varchar(50),
@GrdColGrp_IsDefaultView bit,
@GrdColGrp_IsActiveRow bit,
@GrdColGrp_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcGridColumnGroup'
      INSERT INTO wcGridColumnGroup
          (
		GrdColGrp_Id,
		GrdColGrp_Tb_Id,
		GrdColGrp_Sort,
		GrdColGrp_Name,
		GrdColGrp_HeaderText,
		GrdColGrp_IsDefaultView,
		GrdColGrp_IsActiveRow,
		GrdColGrp_CreatedUserId,
		GrdColGrp_CreatedDate,
		GrdColGrp_UserId,
		GrdColGrp_LastModifiedDate
				)
VALUES	(
		@GrdColGrp_Id, 
		@GrdColGrp_Tb_Id, 
		@GrdColGrp_Sort, 
		RTRIM(LTRIM(@GrdColGrp_Name)), 
		RTRIM(LTRIM(@GrdColGrp_HeaderText)), 
		@GrdColGrp_IsDefaultView, 
		@GrdColGrp_IsActiveRow, 
		@GrdColGrp_UserId, 
		GETDATE(), 
		@GrdColGrp_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Id', 'uniqueidentifier', @GrdColGrp_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Tb_Id', 'uniqueidentifier', @GrdColGrp_Tb_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Sort', 'int', @GrdColGrp_Sort, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Name', 'varchar', @GrdColGrp_Name, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_HeaderText', 'varchar', @GrdColGrp_HeaderText, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_IsDefaultView', 'bit', @GrdColGrp_IsDefaultView, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_IsActiveRow', 'bit', @GrdColGrp_IsActiveRow, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_UserId', 'uniqueidentifier', @GrdColGrp_UserId, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 10;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroup_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroup_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroup_put', 'spWcGridColumnGroup_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroup_put', 'spWcGridColumnGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroup_put', 'spWcGridColumnGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroup_put', 'spWcGridColumnGroup_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroup_put', 'spWcGridColumnGroup_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroup_put]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@GrdColGrp_Id uniqueidentifier = NULL OUTPUT,
@GrdColGrp_Tb_Id uniqueidentifier,
@GrdColGrp_Sort int,
@GrdColGrp_Name varchar(50),
@GrdColGrp_HeaderText varchar(50),
@GrdColGrp_IsDefaultView bit,
@GrdColGrp_IsActiveRow bit,
@GrdColGrp_UserId uniqueidentifier,
@GrdColGrp_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@GrdColGrp_Id Is NULL)
	BEGIN
		SET @GrdColGrp_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcGridColumnGroup WHERE GrdColGrp_Id = @GrdColGrp_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcGridColumnGroup_putInsert'
		EXEC spWcGridColumnGroup_putInsert
		@GrdColGrp_Id,
		@GrdColGrp_Tb_Id,
		@GrdColGrp_Sort,
		@GrdColGrp_Name,
		@GrdColGrp_HeaderText,
		@GrdColGrp_IsDefaultView,
		@GrdColGrp_IsActiveRow,
		@GrdColGrp_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @GrdColGrp_Stamp =( SELECT GrdColGrp_Stamp FROM wcGridColumnGroup WHERE (GrdColGrp_Id = @GrdColGrp_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcGridColumnGroup WHERE (GrdColGrp_Id = @GrdColGrp_Id) AND (GrdColGrp_Stamp = @GrdColGrp_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcGridColumnGroup_putUpdate'
				EXEC spWcGridColumnGroup_putUpdate
				@GrdColGrp_Id,
				@GrdColGrp_Tb_Id,
				@GrdColGrp_Sort,
				@GrdColGrp_Name,
				@GrdColGrp_HeaderText,
				@GrdColGrp_IsDefaultView,
				@GrdColGrp_IsActiveRow,
				@GrdColGrp_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @GrdColGrp_Stamp =( SELECT GrdColGrp_Stamp FROM wcGridColumnGroup WHERE (GrdColGrp_Id = @GrdColGrp_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Id', 'uniqueidentifier', @GrdColGrp_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Tb_Id', 'uniqueidentifier', @GrdColGrp_Tb_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Sort', 'int', @GrdColGrp_Sort, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_Name', 'varchar', @GrdColGrp_Name, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_HeaderText', 'varchar', @GrdColGrp_HeaderText, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_IsDefaultView', 'bit', @GrdColGrp_IsDefaultView, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_IsActiveRow', 'bit', @GrdColGrp_IsActiveRow, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrp_UserId', 'uniqueidentifier', @GrdColGrp_UserId, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 10;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroup_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroup_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroup_putIsDeleted', 'spWcGridColumnGroup_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroup_putIsDeleted', 'spWcGridColumnGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroup_putIsDeleted', 'spWcGridColumnGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroup_putIsDeleted', 'spWcGridColumnGroup_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroup_putIsDeleted', 'spWcGridColumnGroup_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroup_putIsDeleted]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcGridColumnGroup
	   SET          GrdColGrp_IsDeleted = @IsDeleted
	   WHERE        (GrdColGrp_Id = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN



-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcGridColumnGroupItem

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroupItem_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroupItem_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroupItem_row', 'spWcGridColumnGroupItem_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroupItem_row', 'spWcGridColumnGroupItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroupItem_row', 'spWcGridColumnGroupItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroupItem_row', 'spWcGridColumnGroupItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroupItem_row', 'spWcGridColumnGroupItem_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroupItem_row]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
GrdColGrpItm_Id,
GrdColGrpItm_Grp_Id,
GrdColGrpItm_TbC_Id,
TbC_Name AS GrdColGrpItm_TbC_Id_TextField,

GrdColGrpItm_IsActiveRow,
GrdColGrpItm_IsDeleted,
GrdColGrpItm_CreatedUserId,
GrdColGrpItm_CreatedDate,
GrdColGrpItm_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

GrdColGrpItm_LastModifiedDate,
GrdColGrpItm_Stamp


FROM 		wcGridColumnGroupItem LEFT OUTER JOIN
                tbPerson ON wcGridColumnGroupItem.GrdColGrpItm_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTableColumn ON wcGridColumnGroupItem.GrdColGrpItm_TbC_Id = wcTableColumn.TbC_Id


WHERE   (GrdColGrpItm_Id = @Id)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroupItem_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroupItem_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroupItem_lst', 'spWcGridColumnGroupItem_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroupItem_lst', 'spWcGridColumnGroupItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroupItem_lst', 'spWcGridColumnGroupItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroupItem_lst', 'spWcGridColumnGroupItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroupItem_lst', 'spWcGridColumnGroupItem_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroupItem_lst]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@GrdColGrpItm_Grp_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
GrdColGrpItm_Id,
GrdColGrpItm_Grp_Id,
GrdColGrpItm_TbC_Id,
TbC_Name AS GrdColGrpItm_TbC_Id_TextField,

GrdColGrpItm_IsActiveRow,
GrdColGrpItm_IsDeleted,
GrdColGrpItm_CreatedUserId,
GrdColGrpItm_CreatedDate,
GrdColGrpItm_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

GrdColGrpItm_LastModifiedDate,
GrdColGrpItm_Stamp


FROM 		wcGridColumnGroupItem LEFT OUTER JOIN
                tbPerson ON wcGridColumnGroupItem.GrdColGrpItm_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTableColumn ON wcGridColumnGroupItem.GrdColGrpItm_TbC_Id = wcTableColumn.TbC_Id


WHERE   (GrdColGrpItm_Grp_Id = @GrdColGrpItm_Grp_Id) AND (GrdColGrpItm_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroupItem_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroupItem_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroupItem_lstAll', 'spWcGridColumnGroupItem_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroupItem_lstAll', 'spWcGridColumnGroupItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroupItem_lstAll', 'spWcGridColumnGroupItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroupItem_lstAll', 'spWcGridColumnGroupItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroupItem_lstAll', 'spWcGridColumnGroupItem_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroupItem_lstAll]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
AS
SET NOCOUNT ON

SELECT 	
GrdColGrpItm_Id,
GrdColGrpItm_Grp_Id,
GrdColGrpItm_TbC_Id,
TbC_Name AS GrdColGrpItm_TbC_Id_TextField,

GrdColGrpItm_IsActiveRow,
GrdColGrpItm_IsDeleted,
GrdColGrpItm_CreatedUserId,
GrdColGrpItm_CreatedDate,
GrdColGrpItm_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

GrdColGrpItm_LastModifiedDate,
GrdColGrpItm_Stamp


FROM 		wcGridColumnGroupItem LEFT OUTER JOIN
                tbPerson ON wcGridColumnGroupItem.GrdColGrpItm_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTableColumn ON wcGridColumnGroupItem.GrdColGrpItm_TbC_Id = wcTableColumn.TbC_Id

WHERE   (GrdColGrpItm_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroupItem_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroupItem_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroupItem_putUpdate', 'spWcGridColumnGroupItem_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroupItem_putUpdate', 'spWcGridColumnGroupItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroupItem_putUpdate', 'spWcGridColumnGroupItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroupItem_putUpdate', 'spWcGridColumnGroupItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroupItem_putUpdate', 'spWcGridColumnGroupItem_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroupItem_putUpdate]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@GrdColGrpItm_Id uniqueidentifier = NULL OUTPUT,
@GrdColGrpItm_Grp_Id uniqueidentifier,
@GrdColGrpItm_TbC_Id uniqueidentifier,
@GrdColGrpItm_IsActiveRow bit,
@GrdColGrpItm_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcGridColumnGroupItem'
		UPDATE	wcGridColumnGroupItem
		SET
		GrdColGrpItm_TbC_Id = @GrdColGrpItm_TbC_Id,
		GrdColGrpItm_IsActiveRow = @GrdColGrpItm_IsActiveRow,
		GrdColGrpItm_UserId = @GrdColGrpItm_UserId,
		GrdColGrpItm_LastModifiedDate = GETDATE()
		WHERE	(GrdColGrpItm_Id=@GrdColGrpItm_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_Id', 'uniqueidentifier', @GrdColGrpItm_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_Grp_Id', 'uniqueidentifier', @GrdColGrpItm_Grp_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_TbC_Id', 'uniqueidentifier', @GrdColGrpItm_TbC_Id, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_IsActiveRow', 'bit', @GrdColGrpItm_IsActiveRow, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_UserId', 'uniqueidentifier', @GrdColGrpItm_UserId, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 7;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroupItem_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroupItem_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroupItem_putInsert', 'spWcGridColumnGroupItem_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroupItem_putInsert', 'spWcGridColumnGroupItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroupItem_putInsert', 'spWcGridColumnGroupItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroupItem_putInsert', 'spWcGridColumnGroupItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroupItem_putInsert', 'spWcGridColumnGroupItem_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroupItem_putInsert]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@GrdColGrpItm_Id uniqueidentifier = NULL OUTPUT,
@GrdColGrpItm_Grp_Id uniqueidentifier,
@GrdColGrpItm_TbC_Id uniqueidentifier,
@GrdColGrpItm_IsActiveRow bit,
@GrdColGrpItm_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcGridColumnGroupItem'
      INSERT INTO wcGridColumnGroupItem
          (
		GrdColGrpItm_Id,
		GrdColGrpItm_Grp_Id,
		GrdColGrpItm_TbC_Id,
		GrdColGrpItm_IsActiveRow,
		GrdColGrpItm_CreatedUserId,
		GrdColGrpItm_CreatedDate,
		GrdColGrpItm_UserId,
		GrdColGrpItm_LastModifiedDate
				)
VALUES	(
		@GrdColGrpItm_Id, 
		@GrdColGrpItm_Grp_Id, 
		@GrdColGrpItm_TbC_Id, 
		@GrdColGrpItm_IsActiveRow, 
		@GrdColGrpItm_UserId, 
		GETDATE(), 
		@GrdColGrpItm_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_Id', 'uniqueidentifier', @GrdColGrpItm_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_Grp_Id', 'uniqueidentifier', @GrdColGrpItm_Grp_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_TbC_Id', 'uniqueidentifier', @GrdColGrpItm_TbC_Id, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_IsActiveRow', 'bit', @GrdColGrpItm_IsActiveRow, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_UserId', 'uniqueidentifier', @GrdColGrpItm_UserId, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 7;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroupItem_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroupItem_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroupItem_put', 'spWcGridColumnGroupItem_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroupItem_put', 'spWcGridColumnGroupItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroupItem_put', 'spWcGridColumnGroupItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroupItem_put', 'spWcGridColumnGroupItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroupItem_put', 'spWcGridColumnGroupItem_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroupItem_put]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@GrdColGrpItm_Id uniqueidentifier = NULL OUTPUT,
@GrdColGrpItm_Grp_Id uniqueidentifier,
@GrdColGrpItm_TbC_Id uniqueidentifier,
@GrdColGrpItm_IsActiveRow bit,
@GrdColGrpItm_UserId uniqueidentifier,
@GrdColGrpItm_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@GrdColGrpItm_Id Is NULL)
	BEGIN
		SET @GrdColGrpItm_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcGridColumnGroupItem WHERE GrdColGrpItm_Id = @GrdColGrpItm_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcGridColumnGroupItem_putInsert'
		EXEC spWcGridColumnGroupItem_putInsert
		@GrdColGrpItm_Id,
		@GrdColGrpItm_Grp_Id,
		@GrdColGrpItm_TbC_Id,
		@GrdColGrpItm_IsActiveRow,
		@GrdColGrpItm_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @GrdColGrpItm_Stamp =( SELECT GrdColGrpItm_Stamp FROM wcGridColumnGroupItem WHERE (GrdColGrpItm_Id = @GrdColGrpItm_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcGridColumnGroupItem WHERE (GrdColGrpItm_Id = @GrdColGrpItm_Id) AND (GrdColGrpItm_Stamp = @GrdColGrpItm_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcGridColumnGroupItem_putUpdate'
				EXEC spWcGridColumnGroupItem_putUpdate
				@GrdColGrpItm_Id,
				@GrdColGrpItm_Grp_Id,
				@GrdColGrpItm_TbC_Id,
				@GrdColGrpItm_IsActiveRow,
				@GrdColGrpItm_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @GrdColGrpItm_Stamp =( SELECT GrdColGrpItm_Stamp FROM wcGridColumnGroupItem WHERE (GrdColGrpItm_Id = @GrdColGrpItm_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_Id', 'uniqueidentifier', @GrdColGrpItm_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_Grp_Id', 'uniqueidentifier', @GrdColGrpItm_Grp_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_TbC_Id', 'uniqueidentifier', @GrdColGrpItm_TbC_Id, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_IsActiveRow', 'bit', @GrdColGrpItm_IsActiveRow, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@GrdColGrpItm_UserId', 'uniqueidentifier', @GrdColGrpItm_UserId, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 7;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcGridColumnGroupItem_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcGridColumnGroupItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcGridColumnGroupItem_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcGridColumnGroupItem_putIsDeleted', 'spWcGridColumnGroupItem_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcGridColumnGroupItem_putIsDeleted', 'spWcGridColumnGroupItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcGridColumnGroupItem_putIsDeleted', 'spWcGridColumnGroupItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcGridColumnGroupItem_putIsDeleted', 'spWcGridColumnGroupItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcGridColumnGroupItem_putIsDeleted', 'spWcGridColumnGroupItem_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcGridColumnGroupItem_putIsDeleted]
-- Script for this SP was created on: 12/27/2017 6:59:11 PM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcGridColumnGroupItem
	   SET          GrdColGrpItm_IsDeleted = @IsDeleted
	   WHERE        (GrdColGrpItm_Id = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN





--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcGridColumnGroup')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcGridColumnGroup', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO



--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcGridColumnGroupItem')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcGridColumnGroupItem', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


