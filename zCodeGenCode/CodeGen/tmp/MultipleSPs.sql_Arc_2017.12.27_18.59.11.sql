-- wcPropsTileItem


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcPropsTileItem

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTileItem_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTileItem_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTileItem_lst', 'spWcPropsTileItem_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTileItem_lst', 'spWcPropsTileItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTileItem_lst', 'spWcPropsTileItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTileItem_lst', 'spWcPropsTileItem_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTileItem_lst', 'spWcPropsTileItem_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTileItem_lst]
-- Script for this SP was created on: 12/27/2017 5:30:06 PM
(
@PrpTlItm_PrpTl_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
PrpTlItm_Id,
PrpTlItm_PrpTl_Id,
PrpTlItm_Sort,
PrpTlItm_TbC_Id,
TbC_Name AS PrpTlItm_TbC_Id_TextField,

PrpTlItm_IsActiveRow,
PrpTlItm_IsDeleted,
PrpTlItm_CreatedUserId,
PrpTlItm_CreatedDate,
PrpTlItm_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

PrpTlItm_LastModifiedDate,
PrpTlItm_Stamp


FROM 		wcPropsTileItem LEFT OUTER JOIN
                tbPerson ON wcPropsTileItem.PrpTlItm_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTableColumn ON wcPropsTileItem.PrpTlItm_TbC_Id = wcTableColumn.TbC_Id


WHERE   (PrpTlItm_PrpTl_Id = @PrpTlItm_PrpTl_Id) AND (PrpTlItm_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTileItem_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTileItem_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTileItem_row', 'spWcPropsTileItem_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTileItem_row', 'spWcPropsTileItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTileItem_row', 'spWcPropsTileItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTileItem_row', 'spWcPropsTileItem_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTileItem_row', 'spWcPropsTileItem_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTileItem_row]
-- Script for this SP was created on: 12/27/2017 5:30:06 PM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
PrpTlItm_Id,
PrpTlItm_PrpTl_Id,
PrpTlItm_Sort,
PrpTlItm_TbC_Id,
TbC_Name AS PrpTlItm_TbC_Id_TextField,

PrpTlItm_IsActiveRow,
PrpTlItm_IsDeleted,
PrpTlItm_CreatedUserId,
PrpTlItm_CreatedDate,
PrpTlItm_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

PrpTlItm_LastModifiedDate,
PrpTlItm_Stamp


FROM 		wcPropsTileItem LEFT OUTER JOIN
                tbPerson ON wcPropsTileItem.PrpTlItm_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTableColumn ON wcPropsTileItem.PrpTlItm_TbC_Id = wcTableColumn.TbC_Id


WHERE   (PrpTlItm_Id = @Id)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTileItem_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTileItem_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTileItem_lstAll', 'spWcPropsTileItem_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTileItem_lstAll', 'spWcPropsTileItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTileItem_lstAll', 'spWcPropsTileItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTileItem_lstAll', 'spWcPropsTileItem_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTileItem_lstAll', 'spWcPropsTileItem_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTileItem_lstAll]
-- Script for this SP was created on: 12/27/2017 5:30:06 PM
AS
SET NOCOUNT ON

SELECT 	
PrpTlItm_Id,
PrpTlItm_PrpTl_Id,
PrpTlItm_Sort,
PrpTlItm_TbC_Id,
TbC_Name AS PrpTlItm_TbC_Id_TextField,

PrpTlItm_IsActiveRow,
PrpTlItm_IsDeleted,
PrpTlItm_CreatedUserId,
PrpTlItm_CreatedDate,
PrpTlItm_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

PrpTlItm_LastModifiedDate,
PrpTlItm_Stamp


FROM 		wcPropsTileItem LEFT OUTER JOIN
                tbPerson ON wcPropsTileItem.PrpTlItm_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTableColumn ON wcPropsTileItem.PrpTlItm_TbC_Id = wcTableColumn.TbC_Id

WHERE   (PrpTlItm_IsDeleted = 0)




RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTileItem_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTileItem_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTileItem_putInsert', 'spWcPropsTileItem_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTileItem_putInsert', 'spWcPropsTileItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTileItem_putInsert', 'spWcPropsTileItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTileItem_putInsert', 'spWcPropsTileItem_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTileItem_putInsert', 'spWcPropsTileItem_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTileItem_putInsert]
-- Script for this SP was created on: 12/27/2017 5:30:06 PM
(
@PrpTlItm_Id uniqueidentifier = NULL OUTPUT,
@PrpTlItm_PrpTl_Id uniqueidentifier,
@PrpTlItm_Sort int,
@PrpTlItm_TbC_Id uniqueidentifier,
@PrpTlItm_IsActiveRow bit,
@PrpTlItm_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcPropsTileItem'
      INSERT INTO wcPropsTileItem
          (
		PrpTlItm_Id,
		PrpTlItm_PrpTl_Id,
		PrpTlItm_Sort,
		PrpTlItm_TbC_Id,
		PrpTlItm_IsActiveRow,
		PrpTlItm_CreatedUserId,
		PrpTlItm_CreatedDate,
		PrpTlItm_UserId,
		PrpTlItm_LastModifiedDate
				)
VALUES	(
		@PrpTlItm_Id, 
		@PrpTlItm_PrpTl_Id, 
		@PrpTlItm_Sort, 
		@PrpTlItm_TbC_Id, 
		@PrpTlItm_IsActiveRow, 
		@PrpTlItm_UserId, 
		GETDATE(), 
		@PrpTlItm_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_Id', 'uniqueidentifier', @PrpTlItm_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_PrpTl_Id', 'uniqueidentifier', @PrpTlItm_PrpTl_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_Sort', 'int', @PrpTlItm_Sort, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_TbC_Id', 'uniqueidentifier', @PrpTlItm_TbC_Id, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_IsActiveRow', 'bit', @PrpTlItm_IsActiveRow, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_UserId', 'uniqueidentifier', @PrpTlItm_UserId, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 8;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTileItem_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTileItem_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTileItem_putUpdate', 'spWcPropsTileItem_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTileItem_putUpdate', 'spWcPropsTileItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTileItem_putUpdate', 'spWcPropsTileItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTileItem_putUpdate', 'spWcPropsTileItem_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTileItem_putUpdate', 'spWcPropsTileItem_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTileItem_putUpdate]
-- Script for this SP was created on: 12/27/2017 5:30:06 PM
(
@PrpTlItm_Id uniqueidentifier = NULL OUTPUT,
@PrpTlItm_PrpTl_Id uniqueidentifier,
@PrpTlItm_Sort int,
@PrpTlItm_TbC_Id uniqueidentifier,
@PrpTlItm_IsActiveRow bit,
@PrpTlItm_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcPropsTileItem'
		UPDATE	wcPropsTileItem
		SET
		PrpTlItm_Sort = @PrpTlItm_Sort,
		PrpTlItm_TbC_Id = @PrpTlItm_TbC_Id,
		PrpTlItm_IsActiveRow = @PrpTlItm_IsActiveRow,
		PrpTlItm_UserId = @PrpTlItm_UserId,
		PrpTlItm_LastModifiedDate = GETDATE()
		WHERE	(PrpTlItm_Id=@PrpTlItm_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_Id', 'uniqueidentifier', @PrpTlItm_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_PrpTl_Id', 'uniqueidentifier', @PrpTlItm_PrpTl_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_Sort', 'int', @PrpTlItm_Sort, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_TbC_Id', 'uniqueidentifier', @PrpTlItm_TbC_Id, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_IsActiveRow', 'bit', @PrpTlItm_IsActiveRow, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_UserId', 'uniqueidentifier', @PrpTlItm_UserId, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 8;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTileItem_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTileItem_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTileItem_put', 'spWcPropsTileItem_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTileItem_put', 'spWcPropsTileItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTileItem_put', 'spWcPropsTileItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTileItem_put', 'spWcPropsTileItem_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTileItem_put', 'spWcPropsTileItem_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTileItem_put]
-- Script for this SP was created on: 12/27/2017 5:30:06 PM
(
@PrpTlItm_Id uniqueidentifier = NULL OUTPUT,
@PrpTlItm_PrpTl_Id uniqueidentifier,
@PrpTlItm_Sort int,
@PrpTlItm_TbC_Id uniqueidentifier,
@PrpTlItm_IsActiveRow bit,
@PrpTlItm_UserId uniqueidentifier,
@PrpTlItm_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@PrpTlItm_Id Is NULL)
	BEGIN
		SET @PrpTlItm_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcPropsTileItem WHERE PrpTlItm_Id = @PrpTlItm_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcPropsTileItem_putInsert'
		EXEC spWcPropsTileItem_putInsert
		@PrpTlItm_Id,
		@PrpTlItm_PrpTl_Id,
		@PrpTlItm_Sort,
		@PrpTlItm_TbC_Id,
		@PrpTlItm_IsActiveRow,
		@PrpTlItm_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @PrpTlItm_Stamp =( SELECT PrpTlItm_Stamp FROM wcPropsTileItem WHERE (PrpTlItm_Id = @PrpTlItm_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcPropsTileItem WHERE (PrpTlItm_Id = @PrpTlItm_Id) AND (PrpTlItm_Stamp = @PrpTlItm_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcPropsTileItem_putUpdate'
				EXEC spWcPropsTileItem_putUpdate
				@PrpTlItm_Id,
				@PrpTlItm_PrpTl_Id,
				@PrpTlItm_Sort,
				@PrpTlItm_TbC_Id,
				@PrpTlItm_IsActiveRow,
				@PrpTlItm_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @PrpTlItm_Stamp =( SELECT PrpTlItm_Stamp FROM wcPropsTileItem WHERE (PrpTlItm_Id = @PrpTlItm_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_Id', 'uniqueidentifier', @PrpTlItm_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_PrpTl_Id', 'uniqueidentifier', @PrpTlItm_PrpTl_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_Sort', 'int', @PrpTlItm_Sort, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_TbC_Id', 'uniqueidentifier', @PrpTlItm_TbC_Id, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_IsActiveRow', 'bit', @PrpTlItm_IsActiveRow, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@PrpTlItm_UserId', 'uniqueidentifier', @PrpTlItm_UserId, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 8;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcPropsTileItem_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcPropsTileItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcPropsTileItem_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcPropsTileItem_putIsDeleted', 'spWcPropsTileItem_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcPropsTileItem_putIsDeleted', 'spWcPropsTileItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcPropsTileItem_putIsDeleted', 'spWcPropsTileItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcPropsTileItem_putIsDeleted', 'spWcPropsTileItem_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcPropsTileItem_putIsDeleted', 'spWcPropsTileItem_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcPropsTileItem_putIsDeleted]
-- Script for this SP was created on: 12/27/2017 5:30:06 PM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcPropsTileItem
	   SET          PrpTlItm_IsDeleted = @IsDeleted
	   WHERE        (PrpTlItm_Id = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN





--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcPropsTileItem')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcPropsTileItem', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


