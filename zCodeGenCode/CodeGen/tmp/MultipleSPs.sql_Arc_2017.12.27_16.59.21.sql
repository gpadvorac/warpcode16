

/*

SELECT name
FROM OPENROWSET
(
  'SQLNCLI', 
  'Server=Voyager3;Trusted_Connection=yes;',
 'select WarpCodeV16'
); 

*/


-- wcTableSortBy


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcTableSortBy

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTableSortBy_row
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_row]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_row__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_row__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTableSortBy_row__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTableSortBy_row', 'spWcTableSortBy_row__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTableSortBy_row', 'spWcTableSortBy_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTableSortBy_row', 'spWcTableSortBy_row__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTableSortBy_row', 'spWcTableSortBy_row__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTableSortBy_row', 'spWcTableSortBy_row__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTableSortBy_row]
-- Script for this SP was created on: 12/27/2017 1:04:45 PM
(
@Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
TbSb_Id,
TbSb_Tb_Id,
TbSb_TbC_Id,
TbC_Name AS TbSb_TbC_Id_TextField,

TbSb_SortOrder,
TbSb_Direction,
TbSbDir_Name AS TbSb_Direction_TextField,

TbSb_IsActiveRow,
TbSb_IsDeleted,
TbSb_CreatedUserId,
TbSb_CreatedDate,
TbSb_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

TbSb_LastModifiedDate,
TbSb_Stamp


FROM 		wcTableSortBy LEFT OUTER JOIN
                tbPerson ON wcTableSortBy.TbSb_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTableColumn ON wcTableSortBy.TbSb_TbC_Id = wcTableColumn.TbC_Id LEFT OUTER JOIN
                wcTableSortByDirection ON wcTableSortBy.TbSb_Direction = wcTableSortByDirection.TbSbDir_Name


WHERE   (TbSb_Id = @Id)

ORDER BY    TbSb_SortOrder ASC


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTableSortBy_lst
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_lst]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_lst__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_lst__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTableSortBy_lst__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTableSortBy_lst', 'spWcTableSortBy_lst__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTableSortBy_lst', 'spWcTableSortBy_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTableSortBy_lst', 'spWcTableSortBy_lst__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTableSortBy_lst', 'spWcTableSortBy_lst__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTableSortBy_lst', 'spWcTableSortBy_lst__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTableSortBy_lst]
-- Script for this SP was created on: 12/27/2017 1:04:45 PM
(
@TbSb_Tb_Id uniqueidentifier
)
AS
SET NOCOUNT ON

SELECT 	
TbSb_Id,
TbSb_Tb_Id,
TbSb_TbC_Id,
TbC_Name AS TbSb_TbC_Id_TextField,

TbSb_SortOrder,
TbSb_Direction,
TbSbDir_Name AS TbSb_Direction_TextField,

TbSb_IsActiveRow,
TbSb_IsDeleted,
TbSb_CreatedUserId,
TbSb_CreatedDate,
TbSb_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

TbSb_LastModifiedDate,
TbSb_Stamp


FROM 		wcTableSortBy LEFT OUTER JOIN
                tbPerson ON wcTableSortBy.TbSb_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTableColumn ON wcTableSortBy.TbSb_TbC_Id = wcTableColumn.TbC_Id LEFT OUTER JOIN
                wcTableSortByDirection ON wcTableSortBy.TbSb_Direction = wcTableSortByDirection.TbSbDir_Name


WHERE   (TbSb_Tb_Id = @TbSb_Tb_Id) AND (TbSb_IsDeleted = 0)

ORDER BY    TbSb_SortOrder ASC


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTableSortBy_lstAll
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_lstAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_lstAll__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTableSortBy_lstAll__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTableSortBy_lstAll', 'spWcTableSortBy_lstAll__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTableSortBy_lstAll', 'spWcTableSortBy_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTableSortBy_lstAll', 'spWcTableSortBy_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTableSortBy_lstAll', 'spWcTableSortBy_lstAll__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTableSortBy_lstAll', 'spWcTableSortBy_lstAll__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTableSortBy_lstAll]
-- Script for this SP was created on: 12/27/2017 1:04:46 PM
AS
SET NOCOUNT ON

SELECT 	
TbSb_Id,
TbSb_Tb_Id,
TbSb_TbC_Id,
TbC_Name AS TbSb_TbC_Id_TextField,

TbSb_SortOrder,
TbSb_Direction,
TbSbDir_Name AS TbSb_Direction_TextField,

TbSb_IsActiveRow,
TbSb_IsDeleted,
TbSb_CreatedUserId,
TbSb_CreatedDate,
TbSb_UserId,
dbo.udf_FormatPersonName_FL(Pn_FName, Pn_LName) AS UserName,

TbSb_LastModifiedDate,
TbSb_Stamp


FROM 		wcTableSortBy LEFT OUTER JOIN
                tbPerson ON wcTableSortBy.TbSb_UserId = tbPerson.Pn_SecurityUserId LEFT OUTER JOIN
                wcTableColumn ON wcTableSortBy.TbSb_TbC_Id = wcTableColumn.TbC_Id LEFT OUTER JOIN
                wcTableSortByDirection ON wcTableSortBy.TbSb_Direction = wcTableSortByDirection.TbSbDir_Name

WHERE   (TbSb_IsDeleted = 0)

ORDER BY    TbSb_SortOrder ASC


RETURN

GO
--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTableSortBy_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTableSortBy_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTableSortBy_putUpdate', 'spWcTableSortBy_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTableSortBy_putUpdate', 'spWcTableSortBy_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTableSortBy_putUpdate', 'spWcTableSortBy_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTableSortBy_putUpdate', 'spWcTableSortBy_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTableSortBy_putUpdate', 'spWcTableSortBy_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTableSortBy_putUpdate]
-- Script for this SP was created on: 12/27/2017 1:04:46 PM
(
@TbSb_Id uniqueidentifier = NULL OUTPUT,
@TbSb_Tb_Id uniqueidentifier,
@TbSb_TbC_Id uniqueidentifier,
@TbSb_SortOrder int,
@TbSb_Direction varchar(4),
@TbSb_IsActiveRow bit,
@TbSb_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcTableSortBy'
		UPDATE	wcTableSortBy
		SET
		TbSb_TbC_Id = @TbSb_TbC_Id,
		TbSb_SortOrder = @TbSb_SortOrder,
		TbSb_Direction = RTRIM(LTRIM(@TbSb_Direction)),
		TbSb_IsActiveRow = @TbSb_IsActiveRow,
		TbSb_UserId = @TbSb_UserId,
		TbSb_LastModifiedDate = GETDATE()
		WHERE	(TbSb_Id=@TbSb_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_Id', 'uniqueidentifier', @TbSb_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_Tb_Id', 'uniqueidentifier', @TbSb_Tb_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_TbC_Id', 'uniqueidentifier', @TbSb_TbC_Id, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_SortOrder', 'int', @TbSb_SortOrder, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_Direction', 'varchar', @TbSb_Direction, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_IsActiveRow', 'bit', @TbSb_IsActiveRow, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_UserId', 'uniqueidentifier', @TbSb_UserId, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 9;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTableSortBy_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTableSortBy_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTableSortBy_putInsert', 'spWcTableSortBy_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTableSortBy_putInsert', 'spWcTableSortBy_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTableSortBy_putInsert', 'spWcTableSortBy_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTableSortBy_putInsert', 'spWcTableSortBy_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTableSortBy_putInsert', 'spWcTableSortBy_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTableSortBy_putInsert]
-- Script for this SP was created on: 12/27/2017 1:04:46 PM
(
@TbSb_Id uniqueidentifier = NULL OUTPUT,
@TbSb_Tb_Id uniqueidentifier,
@TbSb_TbC_Id uniqueidentifier,
@TbSb_SortOrder int,
@TbSb_Direction varchar(4),
@TbSb_IsActiveRow bit,
@TbSb_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcTableSortBy'
      INSERT INTO wcTableSortBy
          (
		TbSb_Id,
		TbSb_Tb_Id,
		TbSb_TbC_Id,
		TbSb_SortOrder,
		TbSb_Direction,
		TbSb_IsActiveRow,
		TbSb_CreatedUserId,
		TbSb_CreatedDate,
		TbSb_UserId,
		TbSb_LastModifiedDate
				)
VALUES	(
		@TbSb_Id, 
		@TbSb_Tb_Id, 
		@TbSb_TbC_Id, 
		@TbSb_SortOrder, 
		RTRIM(LTRIM(@TbSb_Direction)), 
		@TbSb_IsActiveRow, 
		@TbSb_UserId, 
		GETDATE(), 
		@TbSb_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_Id', 'uniqueidentifier', @TbSb_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_Tb_Id', 'uniqueidentifier', @TbSb_Tb_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_TbC_Id', 'uniqueidentifier', @TbSb_TbC_Id, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_SortOrder', 'int', @TbSb_SortOrder, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_Direction', 'varchar', @TbSb_Direction, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_IsActiveRow', 'bit', @TbSb_IsActiveRow, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_UserId', 'uniqueidentifier', @TbSb_UserId, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 9;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTableSortBy_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTableSortBy_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTableSortBy_put', 'spWcTableSortBy_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTableSortBy_put', 'spWcTableSortBy_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTableSortBy_put', 'spWcTableSortBy_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTableSortBy_put', 'spWcTableSortBy_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTableSortBy_put', 'spWcTableSortBy_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTableSortBy_put]
-- Script for this SP was created on: 12/27/2017 1:04:46 PM
(
@TbSb_Id uniqueidentifier = NULL OUTPUT,
@TbSb_Tb_Id uniqueidentifier,
@TbSb_TbC_Id uniqueidentifier,
@TbSb_SortOrder int,
@TbSb_Direction varchar(4),
@TbSb_IsActiveRow bit,
@TbSb_UserId uniqueidentifier,
@TbSb_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@TbSb_Id Is NULL)
	BEGIN
		SET @TbSb_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcTableSortBy WHERE TbSb_Id = @TbSb_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcTableSortBy_putInsert'
		EXEC spWcTableSortBy_putInsert
		@TbSb_Id,
		@TbSb_Tb_Id,
		@TbSb_TbC_Id,
		@TbSb_SortOrder,
		@TbSb_Direction,
		@TbSb_IsActiveRow,
		@TbSb_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @TbSb_Stamp =( SELECT TbSb_Stamp FROM wcTableSortBy WHERE (TbSb_Id = @TbSb_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcTableSortBy WHERE (TbSb_Id = @TbSb_Id) AND (TbSb_Stamp = @TbSb_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcTableSortBy_putUpdate'
				EXEC spWcTableSortBy_putUpdate
				@TbSb_Id,
				@TbSb_Tb_Id,
				@TbSb_TbC_Id,
				@TbSb_SortOrder,
				@TbSb_Direction,
				@TbSb_IsActiveRow,
				@TbSb_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @TbSb_Stamp =( SELECT TbSb_Stamp FROM wcTableSortBy WHERE (TbSb_Id = @TbSb_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_Id', 'uniqueidentifier', @TbSb_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_Tb_Id', 'uniqueidentifier', @TbSb_Tb_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_TbC_Id', 'uniqueidentifier', @TbSb_TbC_Id, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_SortOrder', 'int', @TbSb_SortOrder, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_Direction', 'varchar', @TbSb_Direction, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_IsActiveRow', 'bit', @TbSb_IsActiveRow, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbSb_UserId', 'uniqueidentifier', @TbSb_UserId, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 9;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTableSortBy_putIsDeleted
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putIsDeleted]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putIsDeleted__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableSortBy_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTableSortBy_putIsDeleted__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTableSortBy_putIsDeleted', 'spWcTableSortBy_putIsDeleted__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTableSortBy_putIsDeleted', 'spWcTableSortBy_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTableSortBy_putIsDeleted', 'spWcTableSortBy_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTableSortBy_putIsDeleted', 'spWcTableSortBy_putIsDeleted__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTableSortBy_putIsDeleted', 'spWcTableSortBy_putIsDeleted__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTableSortBy_putIsDeleted]
-- Script for this SP was created on: 12/27/2017 1:04:46 PM
(
@Id UniqueIdentifier,
@IsDeleted bit,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

BEGIN TRANSACTION;
BEGIN TRY
    BEGIN

	   UPDATE       wcTableSortBy
	   SET          TbSb_IsDeleted = @IsDeleted
	   WHERE        (TbSb_Id = @Id)

	END
    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END


END TRY


BEGIN CATCH
    SET @Success = 0
    IF @TranCount = 0
    ROLLBACK TRANSACTION;

    SET @ErrorLogId = NewID()
    INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
    VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
    --  Log Stored Procedure Parameter Values
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Id', 'uniqueidentifier', @Id, 1;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@IsDeleted', 'bit', @IsDeleted, 2;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 3;
    EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 4;

END CATCH
	
RETURN





--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcTableSortBy')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcTableSortBy', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


