-- wcApplicationVersion


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcApplicationVersion

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcApplicationVersion_putInsert
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_putInsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_putInsert__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcApplicationVersion_putInsert__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcApplicationVersion_putInsert', 'spWcApplicationVersion_putInsert__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcApplicationVersion_putInsert', 'spWcApplicationVersion_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcApplicationVersion_putInsert', 'spWcApplicationVersion_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcApplicationVersion_putInsert', 'spWcApplicationVersion_putInsert__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcApplicationVersion_putInsert', 'spWcApplicationVersion_putInsert__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcApplicationVersion_putInsert]
-- Script for this SP was created on: 1/11/2018 6:56:08 PM
(
@ApVrsn_Id uniqueidentifier = NULL OUTPUT,
@ApVrsn_Ap_Id uniqueidentifier,
@ApVrsn_MajorVersion tinyint,
@ApVrsn_MinorVersion tinyint,
@ApVrsn_VersionIteration tinyint,
@ApVrsn_DbName varchar(75),
@ApVrsn_Server varchar(25),
@ApVrsn_UseLegacyConnectionCode bit,
@ApVrsn_SolutionPath varchar(255),
@ApVrsn_DefaultUIAssembly varchar(100),
@ApVrsn_DefaultUINamespace varchar(100),
@ApVrsn_DefaultUIAssemblyPath varchar(255),
@ApVrsn_ProxyAssemblyName varchar(100),
@ApVrsn_ProxyNamespace varchar(100),
@ApVrsn_ProxyAssemblyPath varchar(255),
@ApVrsn_DefaultWireTypeAssembly varchar(100),
@ApVrsn_DefaultWireTypeNamespace varchar(100),
@ApVrsn_DefaultWireTypePath varchar(255),
@ApVrsn_WebServerURL varchar(100),
@ApVrsn_WebsiteCodeFolderPath varchar(255),
@ApVrsn_WebserviceCodeFolderPath varchar(255),
@ApVrsn_StoredProcCodeFolder varchar(255),
@ApVrsn_DataServiceAssemblyName varchar(100),
@ApVrsn_DataServiceNamespace varchar(100),
@ApVrsn_DataServicePath varchar(255),
@ApVrsn_IsMulticultural bit,
@ApVrsn_Notes varchar(500),
@ApVrsn_IsActiveRow bit,
@ApVrsn_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
    IF @TranCount > 0
        SAVE TRANSACTION ProcedureSave;
    ELSE
        BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
      SET @ErrMsg = 'Attempting to insert into wcApplicationVersion'
      INSERT INTO wcApplicationVersion
          (
		ApVrsn_Id,
		ApVrsn_Ap_Id,
		ApVrsn_MajorVersion,
		ApVrsn_MinorVersion,
		ApVrsn_VersionIteration,
		ApVrsn_DbName,
		ApVrsn_Server,
		ApVrsn_UseLegacyConnectionCode,
		ApVrsn_SolutionPath,
		ApVrsn_DefaultUIAssembly,
		ApVrsn_DefaultUINamespace,
		ApVrsn_DefaultUIAssemblyPath,
		ApVrsn_ProxyAssemblyName,
		ApVrsn_ProxyNamespace,
		ApVrsn_ProxyAssemblyPath,
		ApVrsn_DefaultWireTypeAssembly,
		ApVrsn_DefaultWireTypeNamespace,
		ApVrsn_DefaultWireTypePath,
		ApVrsn_WebServerURL,
		ApVrsn_WebsiteCodeFolderPath,
		ApVrsn_WebserviceCodeFolderPath,
		ApVrsn_StoredProcCodeFolder,
		ApVrsn_DataServiceAssemblyName,
		ApVrsn_DataServiceNamespace,
		ApVrsn_DataServicePath,
		ApVrsn_IsMulticultural,
		ApVrsn_Notes,
		ApVrsn_IsActiveRow,
		ApVrsn_CreatedUserId,
		ApVrsn_CreatedDate,
		ApVrsn_UserId,
		ApVrsn_LastModifiedDate
				)
VALUES	(
		@ApVrsn_Id, 
		@ApVrsn_Ap_Id, 
		@ApVrsn_MajorVersion, 
		@ApVrsn_MinorVersion, 
		@ApVrsn_VersionIteration, 
		RTRIM(LTRIM(@ApVrsn_DbName)), 
		RTRIM(LTRIM(@ApVrsn_Server)), 
		@ApVrsn_UseLegacyConnectionCode, 
		RTRIM(LTRIM(@ApVrsn_SolutionPath)), 
		RTRIM(LTRIM(@ApVrsn_DefaultUIAssembly)), 
		RTRIM(LTRIM(@ApVrsn_DefaultUINamespace)), 
		RTRIM(LTRIM(@ApVrsn_DefaultUIAssemblyPath)), 
		RTRIM(LTRIM(@ApVrsn_ProxyAssemblyName)), 
		RTRIM(LTRIM(@ApVrsn_ProxyNamespace)), 
		RTRIM(LTRIM(@ApVrsn_ProxyAssemblyPath)), 
		RTRIM(LTRIM(@ApVrsn_DefaultWireTypeAssembly)), 
		RTRIM(LTRIM(@ApVrsn_DefaultWireTypeNamespace)), 
		RTRIM(LTRIM(@ApVrsn_DefaultWireTypePath)), 
		RTRIM(LTRIM(@ApVrsn_WebServerURL)), 
		RTRIM(LTRIM(@ApVrsn_WebsiteCodeFolderPath)), 
		RTRIM(LTRIM(@ApVrsn_WebserviceCodeFolderPath)), 
		RTRIM(LTRIM(@ApVrsn_StoredProcCodeFolder)), 
		RTRIM(LTRIM(@ApVrsn_DataServiceAssemblyName)), 
		RTRIM(LTRIM(@ApVrsn_DataServiceNamespace)), 
		RTRIM(LTRIM(@ApVrsn_DataServicePath)), 
		@ApVrsn_IsMulticultural, 
		RTRIM(LTRIM(@ApVrsn_Notes)), 
		@ApVrsn_IsActiveRow, 
		@ApVrsn_UserId, 
		GETDATE(), 
		@ApVrsn_UserId, 
		GETDATE()
				)
                
    END

    SET @TranCount = @@TRANCOUNT;
    IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
    ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
    ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data

		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Id', 'uniqueidentifier', @ApVrsn_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Ap_Id', 'uniqueidentifier', @ApVrsn_Ap_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_MajorVersion', 'tinyint', @ApVrsn_MajorVersion, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_MinorVersion', 'tinyint', @ApVrsn_MinorVersion, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_VersionIteration', 'tinyint', @ApVrsn_VersionIteration, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DbName', 'varchar', @ApVrsn_DbName, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Server', 'varchar', @ApVrsn_Server, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_UseLegacyConnectionCode', 'bit', @ApVrsn_UseLegacyConnectionCode, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_SolutionPath', 'varchar', @ApVrsn_SolutionPath, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultUIAssembly', 'varchar', @ApVrsn_DefaultUIAssembly, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultUINamespace', 'varchar', @ApVrsn_DefaultUINamespace, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultUIAssemblyPath', 'varchar', @ApVrsn_DefaultUIAssemblyPath, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_ProxyAssemblyName', 'varchar', @ApVrsn_ProxyAssemblyName, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_ProxyNamespace', 'varchar', @ApVrsn_ProxyNamespace, 14;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_ProxyAssemblyPath', 'varchar', @ApVrsn_ProxyAssemblyPath, 15;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultWireTypeAssembly', 'varchar', @ApVrsn_DefaultWireTypeAssembly, 16;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultWireTypeNamespace', 'varchar', @ApVrsn_DefaultWireTypeNamespace, 17;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultWireTypePath', 'varchar', @ApVrsn_DefaultWireTypePath, 18;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_WebServerURL', 'varchar', @ApVrsn_WebServerURL, 19;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_WebsiteCodeFolderPath', 'varchar', @ApVrsn_WebsiteCodeFolderPath, 20;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_WebserviceCodeFolderPath', 'varchar', @ApVrsn_WebserviceCodeFolderPath, 21;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_StoredProcCodeFolder', 'varchar', @ApVrsn_StoredProcCodeFolder, 22;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DataServiceAssemblyName', 'varchar', @ApVrsn_DataServiceAssemblyName, 23;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DataServiceNamespace', 'varchar', @ApVrsn_DataServiceNamespace, 24;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DataServicePath', 'varchar', @ApVrsn_DataServicePath, 25;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_IsMulticultural', 'bit', @ApVrsn_IsMulticultural, 26;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Notes', 'varchar', @ApVrsn_Notes, 27;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_IsActiveRow', 'bit', @ApVrsn_IsActiveRow, 28;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_UserId', 'uniqueidentifier', @ApVrsn_UserId, 29;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 30;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 31;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcApplicationVersion_putUpdate
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_putUpdate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_putUpdate__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcApplicationVersion_putUpdate__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcApplicationVersion_putUpdate', 'spWcApplicationVersion_putUpdate__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcApplicationVersion_putUpdate', 'spWcApplicationVersion_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcApplicationVersion_putUpdate', 'spWcApplicationVersion_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcApplicationVersion_putUpdate', 'spWcApplicationVersion_putUpdate__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcApplicationVersion_putUpdate', 'spWcApplicationVersion_putUpdate__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcApplicationVersion_putUpdate]
-- Script for this SP was created on: 1/11/2018 6:56:08 PM
(
@ApVrsn_Id uniqueidentifier = NULL OUTPUT,
@ApVrsn_Ap_Id uniqueidentifier,
@ApVrsn_MajorVersion tinyint,
@ApVrsn_MinorVersion tinyint,
@ApVrsn_VersionIteration tinyint,
@ApVrsn_DbName varchar(75),
@ApVrsn_Server varchar(25),
@ApVrsn_UseLegacyConnectionCode bit,
@ApVrsn_SolutionPath varchar(255),
@ApVrsn_DefaultUIAssembly varchar(100),
@ApVrsn_DefaultUINamespace varchar(100),
@ApVrsn_DefaultUIAssemblyPath varchar(255),
@ApVrsn_ProxyAssemblyName varchar(100),
@ApVrsn_ProxyNamespace varchar(100),
@ApVrsn_ProxyAssemblyPath varchar(255),
@ApVrsn_DefaultWireTypeAssembly varchar(100),
@ApVrsn_DefaultWireTypeNamespace varchar(100),
@ApVrsn_DefaultWireTypePath varchar(255),
@ApVrsn_WebServerURL varchar(100),
@ApVrsn_WebsiteCodeFolderPath varchar(255),
@ApVrsn_WebserviceCodeFolderPath varchar(255),
@ApVrsn_StoredProcCodeFolder varchar(255),
@ApVrsn_DataServiceAssemblyName varchar(100),
@ApVrsn_DataServiceNamespace varchar(100),
@ApVrsn_DataServicePath varchar(255),
@ApVrsn_IsMulticultural bit,
@ApVrsn_Notes varchar(500),
@ApVrsn_IsActiveRow bit,
@ApVrsn_UserId uniqueidentifier,
@Success int = NULL OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @TranCount = @@TRANCOUNT
	IF @TranCount > 0
		SAVE TRANSACTION ProcedureSave;
	ELSE
		BEGIN TRANSACTION;
BEGIN TRY
	BEGIN
		SET @ErrMsg = 'Attempting to update wcApplicationVersion'
		UPDATE	wcApplicationVersion
		SET
		ApVrsn_MajorVersion = @ApVrsn_MajorVersion,
		ApVrsn_MinorVersion = @ApVrsn_MinorVersion,
		ApVrsn_VersionIteration = @ApVrsn_VersionIteration,
		ApVrsn_DbName = RTRIM(LTRIM(@ApVrsn_DbName)),
		ApVrsn_Server = RTRIM(LTRIM(@ApVrsn_Server)),
		ApVrsn_UseLegacyConnectionCode = @ApVrsn_UseLegacyConnectionCode,
		ApVrsn_SolutionPath = RTRIM(LTRIM(@ApVrsn_SolutionPath)),
		ApVrsn_DefaultUIAssembly = RTRIM(LTRIM(@ApVrsn_DefaultUIAssembly)),
		ApVrsn_DefaultUINamespace = RTRIM(LTRIM(@ApVrsn_DefaultUINamespace)),
		ApVrsn_DefaultUIAssemblyPath = RTRIM(LTRIM(@ApVrsn_DefaultUIAssemblyPath)),
		ApVrsn_ProxyAssemblyName = RTRIM(LTRIM(@ApVrsn_ProxyAssemblyName)),
		ApVrsn_ProxyNamespace = RTRIM(LTRIM(@ApVrsn_ProxyNamespace)),
		ApVrsn_ProxyAssemblyPath = RTRIM(LTRIM(@ApVrsn_ProxyAssemblyPath)),
		ApVrsn_DefaultWireTypeAssembly = RTRIM(LTRIM(@ApVrsn_DefaultWireTypeAssembly)),
		ApVrsn_DefaultWireTypeNamespace = RTRIM(LTRIM(@ApVrsn_DefaultWireTypeNamespace)),
		ApVrsn_DefaultWireTypePath = RTRIM(LTRIM(@ApVrsn_DefaultWireTypePath)),
		ApVrsn_WebServerURL = RTRIM(LTRIM(@ApVrsn_WebServerURL)),
		ApVrsn_WebsiteCodeFolderPath = RTRIM(LTRIM(@ApVrsn_WebsiteCodeFolderPath)),
		ApVrsn_WebserviceCodeFolderPath = RTRIM(LTRIM(@ApVrsn_WebserviceCodeFolderPath)),
		ApVrsn_StoredProcCodeFolder = RTRIM(LTRIM(@ApVrsn_StoredProcCodeFolder)),
		ApVrsn_DataServiceAssemblyName = RTRIM(LTRIM(@ApVrsn_DataServiceAssemblyName)),
		ApVrsn_DataServiceNamespace = RTRIM(LTRIM(@ApVrsn_DataServiceNamespace)),
		ApVrsn_DataServicePath = RTRIM(LTRIM(@ApVrsn_DataServicePath)),
		ApVrsn_IsMulticultural = @ApVrsn_IsMulticultural,
		ApVrsn_Notes = RTRIM(LTRIM(@ApVrsn_Notes)),
		ApVrsn_IsActiveRow = @ApVrsn_IsActiveRow,
		ApVrsn_UserId = @ApVrsn_UserId,
		ApVrsn_LastModifiedDate = GETDATE()
		WHERE	(ApVrsn_Id=@ApVrsn_Id)
	END

	SET @TranCount = @@TRANCOUNT;
	IF @TranCount = 0
		BEGIN
			ROLLBACK TRANSACTION;
			SET @Success = 0
		END
	ELSE
		BEGIN
			COMMIT TRANSACTION;
			SET @Success = 1
		END
END TRY

BEGIN CATCH
	SET @Success = 0
	IF @TranCount = 0
		ROLLBACK TRANSACTION;
	ELSE
		BEGIN
			IF XACT_STATE() <> -1
				BEGIN
					ROLLBACK TRANSACTION ProcedureSave;
				END
		END
		-- Log Error Data
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Id', 'uniqueidentifier', @ApVrsn_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Ap_Id', 'uniqueidentifier', @ApVrsn_Ap_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_MajorVersion', 'tinyint', @ApVrsn_MajorVersion, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_MinorVersion', 'tinyint', @ApVrsn_MinorVersion, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_VersionIteration', 'tinyint', @ApVrsn_VersionIteration, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DbName', 'varchar', @ApVrsn_DbName, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Server', 'varchar', @ApVrsn_Server, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_UseLegacyConnectionCode', 'bit', @ApVrsn_UseLegacyConnectionCode, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_SolutionPath', 'varchar', @ApVrsn_SolutionPath, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultUIAssembly', 'varchar', @ApVrsn_DefaultUIAssembly, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultUINamespace', 'varchar', @ApVrsn_DefaultUINamespace, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultUIAssemblyPath', 'varchar', @ApVrsn_DefaultUIAssemblyPath, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_ProxyAssemblyName', 'varchar', @ApVrsn_ProxyAssemblyName, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_ProxyNamespace', 'varchar', @ApVrsn_ProxyNamespace, 14;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_ProxyAssemblyPath', 'varchar', @ApVrsn_ProxyAssemblyPath, 15;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultWireTypeAssembly', 'varchar', @ApVrsn_DefaultWireTypeAssembly, 16;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultWireTypeNamespace', 'varchar', @ApVrsn_DefaultWireTypeNamespace, 17;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultWireTypePath', 'varchar', @ApVrsn_DefaultWireTypePath, 18;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_WebServerURL', 'varchar', @ApVrsn_WebServerURL, 19;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_WebsiteCodeFolderPath', 'varchar', @ApVrsn_WebsiteCodeFolderPath, 20;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_WebserviceCodeFolderPath', 'varchar', @ApVrsn_WebserviceCodeFolderPath, 21;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_StoredProcCodeFolder', 'varchar', @ApVrsn_StoredProcCodeFolder, 22;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DataServiceAssemblyName', 'varchar', @ApVrsn_DataServiceAssemblyName, 23;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DataServiceNamespace', 'varchar', @ApVrsn_DataServiceNamespace, 24;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DataServicePath', 'varchar', @ApVrsn_DataServicePath, 25;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_IsMulticultural', 'bit', @ApVrsn_IsMulticultural, 26;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Notes', 'varchar', @ApVrsn_Notes, 27;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_IsActiveRow', 'bit', @ApVrsn_IsActiveRow, 28;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_UserId', 'uniqueidentifier', @ApVrsn_UserId, 29;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 30;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 31;

END CATCH
	
RETURN
GO

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcApplicationVersion_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcApplicationVersion_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcApplicationVersion_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcApplicationVersion_put', 'spWcApplicationVersion_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcApplicationVersion_put', 'spWcApplicationVersion_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcApplicationVersion_put', 'spWcApplicationVersion_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcApplicationVersion_put', 'spWcApplicationVersion_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcApplicationVersion_put', 'spWcApplicationVersion_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcApplicationVersion_put]
-- Script for this SP was created on: 1/11/2018 6:56:08 PM
(
@ApVrsn_Id uniqueidentifier = NULL OUTPUT,
@ApVrsn_Ap_Id uniqueidentifier,
@ApVrsn_MajorVersion tinyint,
@ApVrsn_MinorVersion tinyint,
@ApVrsn_VersionIteration tinyint,
@ApVrsn_DbName varchar(75),
@ApVrsn_Server varchar(25),
@ApVrsn_UseLegacyConnectionCode bit,
@ApVrsn_SolutionPath varchar(255),
@ApVrsn_DefaultUIAssembly varchar(100),
@ApVrsn_DefaultUINamespace varchar(100),
@ApVrsn_DefaultUIAssemblyPath varchar(255),
@ApVrsn_ProxyAssemblyName varchar(100),
@ApVrsn_ProxyNamespace varchar(100),
@ApVrsn_ProxyAssemblyPath varchar(255),
@ApVrsn_DefaultWireTypeAssembly varchar(100),
@ApVrsn_DefaultWireTypeNamespace varchar(100),
@ApVrsn_DefaultWireTypePath varchar(255),
@ApVrsn_WebServerURL varchar(100),
@ApVrsn_WebsiteCodeFolderPath varchar(255),
@ApVrsn_WebserviceCodeFolderPath varchar(255),
@ApVrsn_StoredProcCodeFolder varchar(255),
@ApVrsn_DataServiceAssemblyName varchar(100),
@ApVrsn_DataServiceNamespace varchar(100),
@ApVrsn_DataServicePath varchar(255),
@ApVrsn_IsMulticultural bit,
@ApVrsn_Notes varchar(500),
@ApVrsn_IsActiveRow bit,
@ApVrsn_UserId uniqueidentifier,
@ApVrsn_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@ApVrsn_Id Is NULL)
	BEGIN
		SET @ApVrsn_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcApplicationVersion WHERE ApVrsn_Id = @ApVrsn_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcApplicationVersion_putInsert'
		EXEC spWcApplicationVersion_putInsert
		@ApVrsn_Id,
		@ApVrsn_Ap_Id,
		@ApVrsn_MajorVersion,
		@ApVrsn_MinorVersion,
		@ApVrsn_VersionIteration,
		@ApVrsn_DbName,
		@ApVrsn_Server,
		@ApVrsn_UseLegacyConnectionCode,
		@ApVrsn_SolutionPath,
		@ApVrsn_DefaultUIAssembly,
		@ApVrsn_DefaultUINamespace,
		@ApVrsn_DefaultUIAssemblyPath,
		@ApVrsn_ProxyAssemblyName,
		@ApVrsn_ProxyNamespace,
		@ApVrsn_ProxyAssemblyPath,
		@ApVrsn_DefaultWireTypeAssembly,
		@ApVrsn_DefaultWireTypeNamespace,
		@ApVrsn_DefaultWireTypePath,
		@ApVrsn_WebServerURL,
		@ApVrsn_WebsiteCodeFolderPath,
		@ApVrsn_WebserviceCodeFolderPath,
		@ApVrsn_StoredProcCodeFolder,
		@ApVrsn_DataServiceAssemblyName,
		@ApVrsn_DataServiceNamespace,
		@ApVrsn_DataServicePath,
		@ApVrsn_IsMulticultural,
		@ApVrsn_Notes,
		@ApVrsn_IsActiveRow,
		@ApVrsn_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @ApVrsn_Stamp =( SELECT ApVrsn_Stamp FROM wcApplicationVersion WHERE (ApVrsn_Id = @ApVrsn_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcApplicationVersion WHERE (ApVrsn_Id = @ApVrsn_Id) AND (ApVrsn_Stamp = @ApVrsn_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcApplicationVersion_putUpdate'
				EXEC spWcApplicationVersion_putUpdate
				@ApVrsn_Id,
				@ApVrsn_Ap_Id,
				@ApVrsn_MajorVersion,
				@ApVrsn_MinorVersion,
				@ApVrsn_VersionIteration,
				@ApVrsn_DbName,
				@ApVrsn_Server,
				@ApVrsn_UseLegacyConnectionCode,
				@ApVrsn_SolutionPath,
				@ApVrsn_DefaultUIAssembly,
				@ApVrsn_DefaultUINamespace,
				@ApVrsn_DefaultUIAssemblyPath,
				@ApVrsn_ProxyAssemblyName,
				@ApVrsn_ProxyNamespace,
				@ApVrsn_ProxyAssemblyPath,
				@ApVrsn_DefaultWireTypeAssembly,
				@ApVrsn_DefaultWireTypeNamespace,
				@ApVrsn_DefaultWireTypePath,
				@ApVrsn_WebServerURL,
				@ApVrsn_WebsiteCodeFolderPath,
				@ApVrsn_WebserviceCodeFolderPath,
				@ApVrsn_StoredProcCodeFolder,
				@ApVrsn_DataServiceAssemblyName,
				@ApVrsn_DataServiceNamespace,
				@ApVrsn_DataServicePath,
				@ApVrsn_IsMulticultural,
				@ApVrsn_Notes,
				@ApVrsn_IsActiveRow,
				@ApVrsn_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @ApVrsn_Stamp =( SELECT ApVrsn_Stamp FROM wcApplicationVersion WHERE (ApVrsn_Id = @ApVrsn_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Id', 'uniqueidentifier', @ApVrsn_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Ap_Id', 'uniqueidentifier', @ApVrsn_Ap_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_MajorVersion', 'tinyint', @ApVrsn_MajorVersion, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_MinorVersion', 'tinyint', @ApVrsn_MinorVersion, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_VersionIteration', 'tinyint', @ApVrsn_VersionIteration, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DbName', 'varchar', @ApVrsn_DbName, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Server', 'varchar', @ApVrsn_Server, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_UseLegacyConnectionCode', 'bit', @ApVrsn_UseLegacyConnectionCode, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_SolutionPath', 'varchar', @ApVrsn_SolutionPath, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultUIAssembly', 'varchar', @ApVrsn_DefaultUIAssembly, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultUINamespace', 'varchar', @ApVrsn_DefaultUINamespace, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultUIAssemblyPath', 'varchar', @ApVrsn_DefaultUIAssemblyPath, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_ProxyAssemblyName', 'varchar', @ApVrsn_ProxyAssemblyName, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_ProxyNamespace', 'varchar', @ApVrsn_ProxyNamespace, 14;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_ProxyAssemblyPath', 'varchar', @ApVrsn_ProxyAssemblyPath, 15;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultWireTypeAssembly', 'varchar', @ApVrsn_DefaultWireTypeAssembly, 16;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultWireTypeNamespace', 'varchar', @ApVrsn_DefaultWireTypeNamespace, 17;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DefaultWireTypePath', 'varchar', @ApVrsn_DefaultWireTypePath, 18;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_WebServerURL', 'varchar', @ApVrsn_WebServerURL, 19;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_WebsiteCodeFolderPath', 'varchar', @ApVrsn_WebsiteCodeFolderPath, 20;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_WebserviceCodeFolderPath', 'varchar', @ApVrsn_WebserviceCodeFolderPath, 21;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_StoredProcCodeFolder', 'varchar', @ApVrsn_StoredProcCodeFolder, 22;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DataServiceAssemblyName', 'varchar', @ApVrsn_DataServiceAssemblyName, 23;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DataServiceNamespace', 'varchar', @ApVrsn_DataServiceNamespace, 24;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_DataServicePath', 'varchar', @ApVrsn_DataServicePath, 25;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_IsMulticultural', 'bit', @ApVrsn_IsMulticultural, 26;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_Notes', 'varchar', @ApVrsn_Notes, 27;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_IsActiveRow', 'bit', @ApVrsn_IsActiveRow, 28;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ApVrsn_UserId', 'uniqueidentifier', @ApVrsn_UserId, 29;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 30;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 31;

END CATCH
	
RETURN
GO


--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcApplicationVersion')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcApplicationVersion', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


