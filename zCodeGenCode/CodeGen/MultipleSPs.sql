-- wcTableChild


-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
-- ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   ***   X   
--     wcTableChild

--==========================================================================================================================================
--==========================================================================================================================================
-- spWcTableChild_put
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableChild_put]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	BEGIN --1
	 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableChild_put__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
		 BEGIN --2
			 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableChild_put__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
				 BEGIN --3
					 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableChild_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
						 BEGIN --4
							 IF EXISTS (SELECT * FROM dbo.sysobjects WHERE ID = OBJECT_ID(N'[spWcTableChild_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
								 BEGIN --5
									 DROP PROCEDURE [spWcTableChild_put__FLAG_DELETE_ME]
									 EXEC sp_rename 'spWcTableChild_put', 'spWcTableChild_put__FLAG_DELETE_ME'
								 END
							 ELSE
								 BEGIN
									 EXEC sp_rename 'spWcTableChild_put', 'spWcTableChild_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
								 END --5
						 END
					 ELSE
						 BEGIN
							 EXEC sp_rename 'spWcTableChild_put', 'spWcTableChild_put__FLAG_DELETE_ME__FLAG_DELETE_ME__FLAG_DELETE_ME'
						 END --4
				 END
			 ELSE
				 BEGIN
					 EXEC sp_rename 'spWcTableChild_put', 'spWcTableChild_put__FLAG_DELETE_ME__FLAG_DELETE_ME'
				 END --3
		 END
	 ELSE
		 BEGIN
			 EXEC sp_rename 'spWcTableChild_put', 'spWcTableChild_put__FLAG_DELETE_ME'
		 END --2
	 END
GO
CREATE PROCEDURE [spWcTableChild_put]
-- Script for this SP was created on: 1/16/2018 11:52:50 AM
(
@TbChd_Id uniqueidentifier = NULL OUTPUT,
@TbChd_Parent_Tb_Id uniqueidentifier,
@TbChd_SortOrder int,
@TbChd_Child_Tb_Id uniqueidentifier,
@TbChd_ChildForeignKeyColumn_Id uniqueidentifier,
@TbChd_ChildForeignKeyColumn22_Id uniqueidentifier,
@TbChd_ChildForeignKeyColumn33_Id uniqueidentifier,
@TbChd_DifferentCombo_Id uniqueidentifier,
@TbChd_IsDisplayAsChildGrid bit,
@TbChd_IsDisplayAsChildTab bit,
@TbChd_IsActiveRow bit,
@TbChd_UserId uniqueidentifier,
@TbChd_Stamp timestamp = NULL OUTPUT,
@BypassConcurrencyCheck bit,
@Success int = NULL OUTPUT,
@IsConcurrencyGood bit = -1 OUTPUT,
@ErrorLogId uniqueidentifier = NULL OUTPUT
)
AS
SET NOCOUNT ON
DECLARE
@IsNew int,
@TranCount int,
@ErrMsg varchar(500);

SET @Success = 0;
SET @IsNew = 0;
SET @IsConcurrencyGood = -1;

BEGIN TRY

IF (@TbChd_Id Is NULL)
	BEGIN
		SET @TbChd_Id = NEWID();
		SET @IsNew = 1;
	END
ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM wcTableChild WHERE TbChd_Id = @TbChd_Id)= 0
			BEGIN
				SET @IsNew = 1;
			END
		ELSE
			BEGIN
				SET @IsNew = 0;
			END
	END		
	
IF @IsNew = 1
	BEGIN
		SET @ErrMsg = 'Attempting to execute spWcTableChild_putInsert'
		EXEC spWcTableChild_putInsert
		@TbChd_Id,
		@TbChd_Parent_Tb_Id,
		@TbChd_SortOrder,
		@TbChd_Child_Tb_Id,
		@TbChd_ChildForeignKeyColumn_Id,
		@TbChd_ChildForeignKeyColumn22_Id,
		@TbChd_ChildForeignKeyColumn33_Id,
		@TbChd_DifferentCombo_Id,
		@TbChd_IsDisplayAsChildGrid,
		@TbChd_IsDisplayAsChildTab,
		@TbChd_IsActiveRow,
		@TbChd_UserId,

		@Success OUTPUT,
		@ErrorLogId OUTPUT
		-- All went well so get the new TimeStamp now
		SET @TbChd_Stamp =( SELECT TbChd_Stamp FROM wcTableChild WHERE (TbChd_Id = @TbChd_Id))
	END
ELSE
	BEGIN
		-- Check for concurrency issue
		IF  ( (@BypassConcurrencyCheck = 0) AND  ( (SELECT COUNT(*) AS Cnt FROM wcTableChild WHERE (TbChd_Id = @TbChd_Id) AND (TbChd_Stamp = @TbChd_Stamp)) = 0 ))
			BEGIN
				-- @BypassConcurrencyCheck was set to False  and  The row had been changed since this data was originaly pulled for updating - Concurrency was not OK
				SET @IsConcurrencyGood = 0;
			END
		ELSE
			BEGIN
				SET @ErrMsg = 'Attempting to execute spWcTableChild_putUpdate'
				EXEC spWcTableChild_putUpdate
				@TbChd_Id,
				@TbChd_Parent_Tb_Id,
				@TbChd_SortOrder,
				@TbChd_Child_Tb_Id,
				@TbChd_ChildForeignKeyColumn_Id,
				@TbChd_ChildForeignKeyColumn22_Id,
				@TbChd_ChildForeignKeyColumn33_Id,
				@TbChd_DifferentCombo_Id,
				@TbChd_IsDisplayAsChildGrid,
				@TbChd_IsDisplayAsChildTab,
				@TbChd_IsActiveRow,
				@TbChd_UserId,

				@Success OUTPUT,
				@ErrorLogId OUTPUT
				-- All went well so get the new TimeStamp now
				SET @TbChd_Stamp =( SELECT TbChd_Stamp FROM wcTableChild WHERE (TbChd_Id = @TbChd_Id))
			END
	END
END TRY

BEGIN CATCH
	SET @Success = 0
		SET @ErrorLogId = NewID()
		INSERT INTO tbSysSqlErrorLog(sysSqlEr_Id, sysSqlEr_Number, sysSqlEr_Severity, sysSqlEr_State, sysSqlEr_Line, sysSqlEr_Procedure, sysSqlEr_Message, sysSqlEr_OtherMessage, sysSqlEr_TimeStamp)
		VALUES (@ErrorLogId, error_number(), error_severity(), error_state(), error_line(), error_procedure(), error_message(), @ErrMsg, GETDATE())		
		--  Log Stored Procedure Parameter Values
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_Id', 'uniqueidentifier', @TbChd_Id, 1;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_Parent_Tb_Id', 'uniqueidentifier', @TbChd_Parent_Tb_Id, 2;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_SortOrder', 'int', @TbChd_SortOrder, 3;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_Child_Tb_Id', 'uniqueidentifier', @TbChd_Child_Tb_Id, 4;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_ChildForeignKeyColumn_Id', 'uniqueidentifier', @TbChd_ChildForeignKeyColumn_Id, 5;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_ChildForeignKeyColumn22_Id', 'uniqueidentifier', @TbChd_ChildForeignKeyColumn22_Id, 6;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_ChildForeignKeyColumn33_Id', 'uniqueidentifier', @TbChd_ChildForeignKeyColumn33_Id, 7;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_DifferentCombo_Id', 'uniqueidentifier', @TbChd_DifferentCombo_Id, 8;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_IsDisplayAsChildGrid', 'bit', @TbChd_IsDisplayAsChildGrid, 9;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_IsDisplayAsChildTab', 'bit', @TbChd_IsDisplayAsChildTab, 10;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_IsActiveRow', 'bit', @TbChd_IsActiveRow, 11;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@TbChd_UserId', 'uniqueidentifier', @TbChd_UserId, 12;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@Success', 'int', @Success, 13;
		EXEC spSysSqlErrorLogParams_insert @ErrorLogId, '@ErrorLogId', 'uniqueidentifier', @ErrorLogId, 14;

END CATCH
	
RETURN
GO


--GO

--IF(NOT EXISTS(SELECT 1 FROM v_DatabaseConnectionReference WHERE (v_DbCRf_Key = N'WcTableChild')))
--BEGIN
--	INSERT        TOP (100) PERCENT
--	INTO          v_DatabaseConnectionReference(v_DbCRf_Id, v_DbCRf_Key, v_DbCRf_Value, v_DbCRf_CreatedDate, v_DbCRf_LastModifiedDate)
--	VALUES        (NEWID(), N'WcTableChild', N'MainDBConnection', GETDATE(), GETDATE())
--END

--GO


